import bricks from 'sql-bricks-postgres';
import createFactory from 'oloo-factory-creator';
const settings = require(process.cwd() + '/resources/settings');

const Repository = {
    init(table, fields, connection) {
        this.table = table;
        this.fields = fields;
        this.connection = connection;
    },

     _handleNoResults(e) {
        // if no results, don't count as an error
        if (e.name == 'QueryResultError') {
            return false;
        }

        throw e;
    },

    /**
     * Similar to getAll(), but returns just the first record
     *
     * @param {Object} params - associate array of column names and values to search by
     * @param {Object} order - associate array of column names to sort by and whether ASC (true) or DESC (false)
     * @returns {Promise}
     */
    getOne(params = undefined, order = {}) {
        return this.getAll(params, order, 1, 1).then((rows) => {
            if (rows.length) {
                return rows[0];
            }

            return null;
        });
    },

    /**
     * Return all records within a repository
     *
     * @param {Object} params - associative array of column names and values to search by
     * @param {Object} order - associate array of column names to sort by and whether ASC (true) or DESC (false)
     * @param {Number} [pageNo]
     * @param {Number} [perPage]
     * @returns {Promise}
     */
    getAll(params = undefined, order = {}, pageNo = undefined, perPage = settings.recordsPerPage) {
        if (params === undefined || params.constructor !== Array) {
            params = this._getParams(params);

            if (!params) {
                return Promise.resolve([]);
            }
        }

        let query;

        try {
            query = bricks.select(this.fields).from(this.table).orderBy(this._getOrderColumns(order));
        } catch (e) {
            return Promise.reject(e);
        }

        params.forEach(param => {
            query.where(param);
        });

        if (pageNo) {
            return this._paginate(pageNo, perPage, query, params);
        }

        const sql = query.toParams();

        return this.connection.query(sql.text, sql.values);
    },

    _getParams(params) {
        const inParams = [];

        if (params !== undefined) {
            params = JSON.parse(JSON.stringify(params));
        }

        params = params !== undefined ? {...params} : {};
        var emptyParamArray = false;

        Object.keys(params).forEach(i => {
            const param = params[i];

            // If one of the params is an array, construct an IN query.
            // If the array is empty, return an empty set as there is nothing to match it.
            if (param.constructor === Array && param.length > 0) {
                inParams.push(bricks.in(i, param));
                delete params[i];
            } else if (param.constructor === Array) {
                emptyParamArray = true;
            }
        });

        if (emptyParamArray) {
            return;
        }

        return [params].concat(inParams);
    },

    _paginate(pageNo, perPage, query, params) {
        const offset = (pageNo - 1) * perPage;
        query.limit(perPage).offset(offset);

        const countQuery = bricks.select('COUNT(1)').from(this.table);

        params.forEach(param => {
            countQuery.where(param);
        });

        const sql = query.toParams();
        const countSql = countQuery.toParams();

        return Promise.all([
            this.connection.query(countSql.text, countSql.values),
            this.connection.query(sql.text, sql.values)
        ]).then(result => {
            const count = result[0][0].count;
            const rows = result[1];

            rows.pagination = {
                totalRecords: Number(count),
                totalPages: Math.ceil(count / perPage),
                pageNo: pageNo,
                perPage: perPage
            };

            return rows;
        });
    },

    _getOrderColumns(order) {
        const orderColumns = [];

        Object.keys(order).forEach(field => {
            if (this.fields.indexOf(field) === -1) {
                throw new Error('Incorrect order columns specified');
            }

            const asc = order[field];
            orderColumns.push(field + (asc ? ' ASC' : ' DESC'));
        });

        orderColumns.push('id');

        return orderColumns;
    },

    /**
     * Return a single record within a repository by its unique identifier
     *
     * @param {Number} id
     * @returns {Promise}
     */
    getById(id) {
        return this.getOne({ id: id });
    },

    /**
     * Insert a record and return the insert identifier
     *
     * @param {Object} record
     * @returns {Promise} returning the inserted id
     */
    insert(record) {
        // Make sure that the record to be inserted only has those fields specified
        const insertRecord = {};

        for (let i in record) {
            if (this.fields.indexOf(i) > -1 && i != "id") {
                insertRecord[i] = record[i];
            }

            // Convert empty strings to null - non-string values like dates etc. get cast as '' which crashes the sql
            // query and the amount of times you need to differentiate between '' and null are vanishingly small
            if (insertRecord[i] === '') {
                insertRecord[i] = null;
            }
        }

        const sql = bricks.insert(this.table, insertRecord).returning("id").toParams();

        return this.connection.one(sql.text, sql.values).then((result) => result.id);
    },

    /**
     * Update a record
     *
     * @param {Number} id
     * @param {Object} record
     * @returns {Promise} returning true if there was a valid record to update
     */
    update(id, record) {
        // Make sure that the record to be inserted only has those fields specified
        const updateRecord = {};

        for (let i in record) {
            if (this.fields.indexOf(i) > -1 && i != "id") {
                updateRecord[i] = record[i];
            }

            // Convert empty strings to null - non-string values like dates etc. get cast as '' which crashes the sql
            // query and the amount of times you need to differentiate between '' and null are vanishingly small
            if (updateRecord[i] === '') {
                updateRecord[i] = null;
            }
        }

        const sql = bricks.update(this.table, updateRecord).where({"id": id}).returning("1").toParams();

        return this.connection.one(sql.text, sql.values).then(
            () => true,
            this._handleNoResults
        );
    },

    /**
     * Delete a record
     *
     * @param {Number} id
     * @returns {Promise} returning true if there was a valid record to delete
     */
    delete(id) {
        const sql = bricks.delete(this.table).where({"id": id}).returning("1").toParams();

        return this.connection.one(sql.text, sql.values).then(
            () => true,
            this._handleNoResults
        );
    }
};

export default createFactory(Repository);