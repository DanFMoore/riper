##RIPER

RIPER stands for React Isomorphic PostgreSQL Express Redux. The "Isomorphic" part is React, but I had to rearrange the order of the words to get a better acronym.

This is a library for developing websites using the above technologies.