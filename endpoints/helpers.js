/**
 * Do something with the state assuming there is not a notFound state set.
 *
 * Tested indirectly in endpoints/repositoriesSpec.js
 *
 * @param {Object} res
 * @param {Function} callback to be run if not in notFound state; passed the state
 */
export function useState(res, callback) {
    const state = res.locals.store.getState();

    if (state.common.notFound) {
        res.sendStatus(404);
    } else {
        callback(state);
    }
}

/**
 * Check whether the request should proceed based on validity criteria.
 *
 * Tested indirectly in endpoints/repositoriesSpec.js
 *
 * @param {Object} req
 * @param {Object} res
 * @returns {Boolean}
 */
export function checkValidity(req, res) {
    const requestState = res.locals.store.getState().request;

    if (requestState.invalid) {
        res.status(422);
        res.json(requestState.errors);

        return false;
    }

    // Don't save anything if this is just for validation
    if (req.headers['validate-only']) {
        res.sendStatus(200);

        return false;
    }

    return true;
}