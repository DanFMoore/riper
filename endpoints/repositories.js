import { useState, checkValidity } from "./helpers";

export default function(router, actions) {
    /**
     * Return all records from a repository
     */
    router.get('/:repository', (req, res, next) => {
        const store = res.locals.store;
        const order = req.headers["record-order"] ? JSON.parse(req.headers["record-order"]) : {};

        store.dispatch(actions.getRecords(
            req.params.repository,
            req.query,
            order,
            req.headers['record-page-no'] || undefined,
            req.headers['record-per-page'] || undefined
        )).then(() => {
            useState(res, state => {
                res.set('record-pagination', JSON.stringify(state[req.params.repository].pagination));
                res.json(state[req.params.repository].records);
            });
        }).catch(next);
    });

    /**
     * Return a single record from a repository by its id
     */
    router.get('/:repository/:id', (req, res, next) => {
        const store = res.locals.store;

        store.dispatch(actions.getRecord(req.params.repository, req.params.id)).then(() => {
            useState(res, state => {
                res.json(state[req.params.repository].record);
            });
        }).catch(next);
    });

    /**
     * Create a new record in a repository and send a location to the new record
     */
    router.post('/:repository', (req, res, next) => {
        const store = res.locals.store;
        const requestState = store.getState().request;

        if (!checkValidity(req, res)) {
            return;
        }

        store.dispatch(actions.createRecord(req.params.repository, requestState.body)).then(() => {
            useState(res, state => {
                const createdRecord = state[req.params.repository].createdRecord;

                res.set('Location', '/' + req.params.repository + '/' + createdRecord.id);
                res.sendStatus(201);
            });
        }).catch(next);
    });

    /**
     * Update a record in a repository by its id
     */
    router.put('/:repository/:id', (req, res, next) => {
        const store = res.locals.store;
        const requestState = store.getState().request;

        if (!checkValidity(req, res)) {
            return;
        }

        store.dispatch(actions.updateRecord(req.params.repository, req.params.id, requestState.body)).then(() => {
            useState(res, state => {
                res.sendStatus(204);
            });
        }).catch(next);
    });

    /**
     * Delete a record in a repository by its id
     */
    router.delete('/:repository/:id', (req, res, next) => {
        const store = res.locals.store;

        store.dispatch(actions.deleteRecord(req.params.repository, req.params.id)).then(() => {
            useState(res, state => {
                res.sendStatus(204);
            });
        }).catch(next);
    });

    return router;
};
