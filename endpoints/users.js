import { checkUserPermission } from "../actions/users";
import { validate, filter } from "../actions/server/request";
import * as validators from "../validators/user";
import * as filters from "../actions/filters/user";

export default function(router, actions, asyncValidators) {
    router.post('/login', (req, res, next) => {
        const store = req.res.locals.store;

        store.dispatch(actions.login(req.body.username, req.body.password)).then(() => {
            // This is technically a violation of REST, as post requests are supposed to create a resource,
            // then send a location header, or redirect, to the new resource. But this is easier.
            res.json(store.getState().users);
        }).catch(next);
    });

    // Check the permission when accessing a user by its id
    router.use('/:id', (req, res, next) => {
        if (Number(req.params.id) && req.method !== 'OPTIONS') {
            res.locals.store.dispatch(checkUserPermission(req.params.id));
        }

        next();
    });

    // Update the billing details for the logged in user
    router.put('/billing-details', (req, res, next) => {
        const store = res.locals.store;
        const user = store.getState().users.loggedInUser;

        if (!user) {
            res.send(404);
        }

        req.url = '/' + user.id;

        store.dispatch(validate(validators.address));
        store.dispatch(filter(filters.address)).then(next).catch(next);
    });

    /**
     * Validate and filter the posted user and redirect back to the main repository POST route
     */
    router.post('/', (req, res, next) => {
        const store = res.locals.store;

        store.dispatch(validate(validators.register, asyncValidators.register)).then(() => {
            return store.dispatch(filter(filters.register));
        }).then(next).catch(next);
    });

    return router;
};