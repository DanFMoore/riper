import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import * as redux from 'redux';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { clearError } from './actions/common';

function createStore(reduce, initialState) {
    return redux.createStore(reduce, initialState, redux.applyMiddleware(thunk));
}

/**
 * Render the react redux components into a HTML element in a browser
 * and subscribe to state changes in order to save via AJAX into session
 *
 * @param {ReactElement} App
 * @param {Function} reduce
 * @param {String} apiDomain
 */
export function renderClient(App, reduce, apiDomain) {
    axios.defaults.baseURL = apiDomain;
    axios.defaults.headers['Content-Type']= "application/json";
    axios.defaults.headers['Accept']= "application/json";

    try {
        const token = window.__REDUX_STATE__.users.token;

        if (token) {
            axios.defaults.headers['Authorization'] = token;
        }
    } catch (e) {
        console.error(e);
    }

    const store = createStore(reduce, window.__REDUX_STATE__);
    let oldState = {};

    store.subscribe(function () {
        const state = { ...store.getState() };

        Object.keys(state).forEach((key) => {
            if (!state[key].save) {
                delete state[key];
            }
        });

        if (JSON.stringify(state) !== JSON.stringify(oldState)) {
            axios.put('/redux-state', state, {
                baseURL: document.location.origin
            });

            oldState = state;
        }
    });

    // Allow the passed state to be garbage-collected
    window.__REDUX_STATE__ = undefined;

    ReactDOM.render(<BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>, document.getElementById('react-view'));

    // Bit of a hack to allow me to hook into pushState, which doesn't have an event unlike popState
    const pushState = window.history.pushState;

    window.history.pushState = function () {
        pushState.apply(history, arguments);

        store.dispatch(clearError());
    };

    window.addEventListener('popstate', function () {
        store.dispatch(clearError());
    });
}

/**
 * Catch-all express route for rendering react redux components, which is called either directly by express being
 * routed to a page that doesn't have a route specified in a previous app.use(), or by one of those aforesaid routes
 * calling next after dispatching some required actions.
 *
 * @param {ReactElement} App
 * @param {String} layout the layout view script
 * @param {Object} context - slight hack, only way I could change the context params
 * @return {Function}
 */
export function renderServer(App, layout = 'layout', context = {}) {
    return (req, res) => {
        const store = res.locals.store;

        const staticRouter = <StaticRouter
            location={req.originalUrl}
            context={context}
        >
            <Provider store={store}>
                <App />
            </Provider>
        </StaticRouter>;

        if (context.url) {
            res.redirect(301, context.url);
        } else {
            const html = ReactDOMServer.renderToString(staticRouter);

            if (context.status) {
                res.status(context.status);
            }

            const state = store.getState();

            // Strip XSS vulnerability
            const encodedState = JSON.stringify(state).replace(/\//g, '\\/');
            res.render(layout, {html, state: encodedState, rawState: state});
        }

        return staticRouter;
    };
}

/**
 * Set the redux state into res.locals or save the redux state in the
 * session so that it is persisted when the page is refreshed.
 *
 * @param {Function} reduce
 * @return {Function}
 */
export function reduxStore(reduce) {
    return (req, res, next) => {
        if (req.method === 'PUT' && req.url === '/redux-state') {
            req.session.reduxState = req.body;
            res.send();
        } else {
            res.locals.store = createStore(reduce, req.session.reduxState);
            next();
        }
    }
}

/**
 * Create a redux store that just saves the dispatched actions in order.
 */
export function createTestStore(initialState = {}) {
    initialState = {
        ...initialState,
        actions: []
    };

    function reduce(state = {}, action) {
        if (action.type !== '@@redux/INIT') {
            return {
                ...state,
                actions: [...state.actions, action]
            };
        }

        return state;
    }

    return redux.createStore(reduce, initialState, redux.applyMiddleware(thunk));
}