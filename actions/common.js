import { ERROR, CLEAR_ERROR, NOT_FOUND } from "./types";

export function errorHandler(error) {
    return dispatch => {
        dispatch({
            type: ERROR,
            error
        });

        throw error;
    };
}

export function clearError() {
    return {
        type: CLEAR_ERROR
    };
}

export function notFound() {
    return {
        type: NOT_FOUND
    }
}
