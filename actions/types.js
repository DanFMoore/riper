export const REPOSITORY_GET_RECORDS = 'REPOSITORY_GET_RECORDS';
export const REPOSITORY_GET_RECORD = 'REPOSITORY_GET_RECORD';
export const REPOSITORY_CREATE_RECORD = 'REPOSITORY_CREATE_RECORD';
export const REPOSITORY_UPDATE_RECORD = 'REPOSITORY_UPDATE_RECORD';
export const REPOSITORY_DELETE_RECORD = 'REPOSITORY_DELETE_RECORD';

export const USER_AUTH = 'USER_AUTH';
export const USERNAME_NOT_FOUND = 'USERNAME_NOT_FOUND';
export const USER_TOKEN_EXPIRED = 'USER_TOKEN_EXPIRED';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_REGISTER = 'USER_REGISTER';

export const REQUEST_STORE_BODY = 'REQUEST_STORE_BODY';
export const REQUEST_VALIDATE= 'REQUEST_VALIDATE';

export const MODAL_SHOW = 'MODAL_SHOw';
export const MODAL_HIDE = 'MODAL_HIDE';
export const MODAL_HIDE_ALL = 'MODAL_HIDE_ALL';

export const TITLE_SET = 'TITLE_SET';

export const ERROR = 'ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const NOT_FOUND = 'NOT_FOUND';