import Password from "../../auth/password";
import { REQUEST_STORE_BODY } from "../types";

export function register(user) {
    return dispatch => {
        return Password(user.password).hash().then(hash => {
            dispatch({
                type: REQUEST_STORE_BODY,
                body: {
                    username: user.username,
                    name: user.name,
                    email: user.email,
                    password: hash,
                    joinDate: new Date()
                }
            });
        });
    };
}

export function address(user) {
    return {
        type: REQUEST_STORE_BODY,
        body: {
            name: user.name,
            address1: user.address1,
            address2: user.address2,
            town: user.town,
            county: user.county,
            country: user.country,
            postcode: user.postcode
        }
    }
}