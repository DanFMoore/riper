import * as types from "./types";

/**
 * Reload the records so that if they have had additional related records inserted,
 * related reducers can listen for it again to add as related records to itself,
 * getting the related records' related records without having to "step-through"
 * to get them.
 *
 * For example: orders have product lines and product lines have products.
 * If products are inserted into product lines then product lines into orders,
 * then the order state can have access to the products indirectly without the order
 * reducer having to do any of the work.
 *
 * @param {String} repository
 * @returns {Function}
 */
export function reloadRecords(repository) {
    return (dispatch, getState) => {
        dispatch({
            repository,
            type: types.REPOSITORY_GET_RECORDS,
            records: getState()[repository].records
        });

        return Promise.resolve();
    };
}

export function selectRecord(repository, record) {
    return {
        type: types.REPOSITORY_GET_RECORD,
        repository,
        record
    }
}