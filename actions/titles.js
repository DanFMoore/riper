import * as types from "./types";

export function setTitle(title, html = null) {
    return {
        type: types.TITLE_SET,
        title,
        html
    };
}