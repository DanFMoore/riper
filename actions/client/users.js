import { createRecord } from "./repositories";
import * as types from "../types";
import axios from "axios";

export function login(user) {
    return (dispatch) => {
        return axios.post('/users/login', user).then(response => {
            const data = response.data;

            if (data.token) {
                axios.defaults.headers['Authorization'] = data.token;
            } else {
                delete axios.defaults.headers['Authorization'];
            }

            return dispatch({
                loginFailureMessage: data.loginFailureMessage,
                type: data.actionType,
                token: data.token,
                loggedInUser: data.loggedInUser
            });
        });
    };
}

export function register(user) {
    return (dispatch) => {
        return dispatch(createRecord('users', user)).then(() => {
            return dispatch(login(user));
        }).then(() => {
            return dispatch({
                type: types.USER_REGISTER
            });
        });
    };
}

export function logout() {
    return (dispatch, getState) => {
        if (getState().users.loggedInUser) {
            dispatch({
                type: types.USER_LOGOUT
            });
        }
    };
}

export function saveBillingDetails(user) {
    return (dispatch, getState) => {
        const loggedInUser = getState().users.loggedInUser;

        return axios.put('/users/billing-details', user).then(response => {
            dispatch({
                type: types.REPOSITORY_UPDATE_RECORD,
                repository: 'users',
                record: user,
                id: loggedInUser.id
            });
        });
    };
}