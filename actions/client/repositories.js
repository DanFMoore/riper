import * as types from "../types";
import { errorHandler, notFound } from "../common";
import axios from "axios";

function callAxios(callback) {
    return (dispatch) => {
        return callback(dispatch).catch(error => {
            if (error.response && error.response.status === 404) {
                dispatch(notFound());
            } else {
                dispatch(errorHandler(error));
            }
        });
    };
}

export function getRecords(repository, params = undefined, order = {}, pageNo = undefined, perPage = undefined) {
    return callAxios(dispatch => {
        return axios.get(`/${repository}`, {
            params: params || {},
            headers: {
                "record-order": JSON.stringify(order),
                "record-page-no": pageNo || '',
                "record-per-page": perPage || ''
            }
        }).then(response => {
            dispatch({
                type: types.REPOSITORY_GET_RECORDS,
                repository,
                records: response.data,
                orderColumns: order,
                pagination: JSON.parse(response.headers["record-pagination"])
            });
        });
    });
}

export function getRecord(repository, id) {
    return callAxios(dispatch => {
        return axios.get(`/${repository}/${id}`).then(response => {
            dispatch({
                type: types.REPOSITORY_GET_RECORD,
                repository,
                record: response.data
            });
        });
    });
}

export function getRecordBy(repository, params = undefined, order = {}) {
    return callAxios(dispatch => {
        return axios.get(`/${repository}`, {
            params: params || {},
            headers: {
                "record-order": JSON.stringify(order)
            }
        }).then(response => {
            if (response.data.length) {
                dispatch({
                    type: types.REPOSITORY_GET_RECORD,
                    repository,
                    record: response.data[0]
                });
            } else {
                dispatch(notFound());
            }
        });
    });
}

export function createRecord(repository, record) {
    return callAxios(dispatch => {
        return axios.post(`/${repository}`, record).then(response => {
            const parts = response.headers.location.split('/');
            const id = Number(parts[parts.length - 1]);

            dispatch({
                type: types.REPOSITORY_CREATE_RECORD,
                repository,
                record,
                id
            });
        });
    });
}

export function updateRecord(repository, id, record) {
    return callAxios(dispatch => {
        return axios.put(`/${repository}/${id}`, record).then(response => {
            dispatch({
                type: types.REPOSITORY_UPDATE_RECORD,
                repository,
                record,
                id
            });
        });
    });
}

export function deleteRecord(repository, id, record = null) {
    return callAxios(dispatch => {
        return axios.delete(`/${repository}/${id}`).then(response => {
            dispatch({
                type: types.REPOSITORY_DELETE_RECORD,
                repository,
                record,
                id
            });
        });
    });
}