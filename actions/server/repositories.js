import * as types from "../types";
import { errorHandler, notFound } from "../common";

/**
 * @param repositories
 */
export default function getActions(repositories) {
    function callRepository(repository, callback) {
        return (dispatch) => {
            if (repositories[repository]) {
                return callback(dispatch).catch((error) => {
                    dispatch(errorHandler(error));
                });
            } else {
                dispatch(notFound());

                return Promise.resolve();
            }
        };
    }

    return {
        getRecords(repository, params = undefined, order = {}, pageNo = undefined, perPage = undefined) {
            return callRepository(repository, (dispatch) => {
                return repositories[repository].getAll(params, order, pageNo, perPage).then(records => {
                    dispatch({
                        type: types.REPOSITORY_GET_RECORDS,
                        repository,
                        records: [ ...records ],
                        orderColumns: order,
                        pagination: records.pagination
                    });
                });
            });
        },
        getRecord(repository, id) {
            return callRepository(repository, (dispatch) => {
                return repositories[repository].getById(id).then(record => {
                    if (record) {
                        dispatch({
                            type: types.REPOSITORY_GET_RECORD,
                            repository,
                            record
                        });
                    } else {
                        dispatch(notFound());
                    }
                });
            });
        },
        getRecordBy(repository, params = undefined, order = {}) {
            return callRepository(repository, (dispatch) => {
                return repositories[repository].getOne(params, order).then(record => {
                    if (record) {
                        dispatch({
                            type: types.REPOSITORY_GET_RECORD,
                            repository,
                            record
                        });
                    } else {
                        dispatch(notFound());
                    }
                });
            });
        },
        createRecord(repository, record) {
            return callRepository(repository, (dispatch) => {
                return repositories[repository].insert(record).then(id => {
                    dispatch({
                        type: types.REPOSITORY_CREATE_RECORD,
                        repository,
                        record,
                        id
                    });
                });
            });
        },
        updateRecord(repository, id, record) {
            return callRepository(repository, (dispatch) => {
                return repositories[repository].update(id, record).then(updated => {
                    if (updated) {
                        dispatch({
                            type: types.REPOSITORY_UPDATE_RECORD,
                            repository,
                            record,
                            id
                        });
                    } else {
                        dispatch(notFound());
                    }
                });
            });
        },
        deleteRecord(repository, id, record = null) {
            return callRepository(repository, (dispatch) => {
                return repositories[repository].delete(id).then(updated => {
                    if (updated) {
                        dispatch({
                            type: types.REPOSITORY_DELETE_RECORD,
                            repository,
                            record,
                            id
                        });
                    } else {
                        dispatch(notFound());
                    }
                });
            });
        }
    };
}
