import * as types from "../types";
import Password from '../../auth/password';
import jwt from 'jsonwebtoken';
const settings = require(process.cwd() + '/resources/settings');

export default function getActions(users) {
    return {
        login(username, password) {
            return (dispatch) => {
                return users.getOne({
                    username,
                    active: true,
                }).then(user => {
                    if (user) {
                        return Password(password).authenticate(user.password).then(result => {
                            dispatch({
                                type: types.USER_AUTH,
                                loggedInUser: result ? user : null,
                                token: result ? jwt.sign(
                                    {
                                        id: user.id,
                                        password: user.password
                                    },
                                    settings.jwtSecret
                                ) : null
                            });
                        });
                    } else {
                        dispatch({
                            type: types.USERNAME_NOT_FOUND
                        });
                    }
                });
            };
        },
        getUserByToken(token) {
            return (dispatch) => {
                var payload;

                try {
                    payload = jwt.decode(token, settings.jwtSecret);
                }
                catch (e) {
                    return Promise.reject(e);
                }

                return users.getOne({
                    id: payload.id,
                    password: payload.password,
                    active: true
                }).then(user => {
                    if (user) {
                        dispatch({
                            type: types.USER_AUTH,
                            loggedInUser: user,
                            result: true
                        });
                    } else {
                        dispatch({
                            type: types.USER_TOKEN_EXPIRED
                        })
                    }
                });
            };
        }
    };
}
