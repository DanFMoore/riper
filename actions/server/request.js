import * as types from "../types";

export function storeBody(body) {
    return {
        type: types.REQUEST_STORE_BODY,
        body
    }
}

export function validate(syncValidation, asyncValidation = null, body = null) {
    return (dispatch, getState) => {
        if (body) {
            dispatch(storeBody(body));
        }

        const bodyState = getState().request.body;

        dispatch({
            type: types.REQUEST_VALIDATE,
            errors: syncValidation(bodyState)
        });

        if (asyncValidation) {
            return asyncValidation(bodyState).catch(errors => {
                dispatch({
                    type: types.REQUEST_VALIDATE,
                    errors
                });
            });
        }
    };
}

export function filter(action) {
    return (dispatch, getState) => {
        const state = getState().request;

        if (state.invalid) {
            return Promise.resolve();
        }

        return Promise.resolve(dispatch(action(state.body)));
    }
}