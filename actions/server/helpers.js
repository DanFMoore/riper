const settings = require(process.cwd() + '/resources/settings');

export function getSelectFields(fields, alias) {
    return fields.map(f => `${alias}."${f}"`).join(', ');
}

export function buildJsonObject(fields, alias) {
    return fields.map(f => `'${f}', ${alias}."${f}"`).join(', ');
}

/**
 * Return the records and pagination information for a query
 *
 * @param connection
 * @param fields
 * @param query
 * @param params
 * @param pageNo
 * @param perPage
 * @returns {Promise}
 */
export function executePaginationQuery(
    connection, fields, query, params, pageNo, perPage = settings.recordsPerPage
) {
    const offset = (pageNo - 1) * perPage;
    const recordsQuery = `SELECT ${fields} ${query} LIMIT ${perPage} OFFSET ${offset}`;

    query = query.split('ORDER BY')[0];
    const countQuery = 'SELECT COUNT(1) ' + query;

    return Promise.all([
        connection.query(countQuery, params),
        connection.query(recordsQuery, params)
    ]).then(result => {
        const count = result[0][0].count;
        const records = result[1];

        const pagination = {
            totalRecords: Number(count),
            totalPages: Math.ceil(count / perPage),
            pageNo: pageNo,
            perPage: perPage
        };

        return {
            records,
            pagination
        };
    });
}