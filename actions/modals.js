import * as types from "./types";

export function showModal(modal) {
    return {
        type: types.MODAL_SHOW,
        modal
    }
}

export function hideModal(modal) {
    return {
        type: types.MODAL_HIDE,
        modal
    }
}

export function hideModals() {
    return {
        type: types.MODAL_HIDE_ALL
    }
}