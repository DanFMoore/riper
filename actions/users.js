import { errorHandler } from './common';

export function forbidden() {
    return function forbiddenDispatch(dispatch, getState) {
        var error;

        if (getState().users.loggedInUser) {
            error = new Error('Access forbidden');
            error.status = 403;
        } else {
            error = new Error('Not logged in');
            error.status = 401;
        }

        dispatch(errorHandler(error));
    };
}

/**
 * Check whether the logged in user has access to the requested user.
 * @todo add some sort of user permissions thing. At the moment this blocks users from a single user record,
 * but not the route the returns all of them.
 *
 * @param {Number} userId
 * @returns {function(*, *)}
 */
export function checkUserPermission(userId) {
    return (dispatch, getState) => {
        const user = getState().users.loggedInUser;

        // Non-strict equality check; probably will be a string coming from the express route.
        if (!user || user.id != userId) {
            dispatch(forbidden());
        }
    };
}