import createFactory from 'oloo-factory-creator';
import pbkdf2 from 'easy-pbkdf2';

const pbk = pbkdf2({
    "DEFAULT_HASH_ITERATIONS": 50000,
    "SALT_SIZE": 64
});

/**
 * Just a wrapper around the easy-pbkdf2 library to introduce promises, have an OOP syntax,
 * set up the defaults and combine the hash and salt into one easy to manage field.
 *
 * @type {{hash, authenticate}}
 */
const Password = {
    /**
     * Initialise the password
     *
     * @param {String} passwordString
     */
    init(passwordString) {
        this.passwordString = passwordString;
    },

    /**
     * Hash the password and return a $ separated string containing the hash and salt
     *
     * @return {Promise}
     */
    hash() {
        return new Promise((resolve, reject) => {
            var salt = pbk.generateSalt();

            pbk.secureHash(this.passwordString, salt, function (err, hash) {
                err ? reject(err) : resolve(`${hash}$${salt}`);
            });
        });
    },

    /**
     * When given a $ separated hash and salt, authenticate against the password string
     *
     * @param {String} hash
     * @return {Promise}
     */
    authenticate(hash) {
        return new Promise((resolve, reject) => {
            var hashParts = hash.split('$');

            pbk.verify(hashParts[1], hashParts[0], this.passwordString, function (err, valid) {
                err ? reject(err) : resolve(valid);
            });
        });
    }
};

/**
 * Create a password object form the supplied password string
 *
 * @param {String} passwordString
 * @returns {{hash, authenticate}}
 */
export default createFactory(Password);
