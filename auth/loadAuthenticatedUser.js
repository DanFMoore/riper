/**
 * Loads the authenticated user, if any, into the state.
 * If there are auth headers, but they are incorrect, then send a 401 and don't continue.
 *
 * @param {Object} actions
 */
export default actions => (req, res, next) => {
    const store = res.locals.store;

    if (req.headers.authorization) {
        store.dispatch(actions.getUserByToken(req.headers.authorization)).then(() => {
            if (store.getState().users.loggedInUser) {
                next();
            } else {
                res.sendStatus(401);
            }
        }).catch(next);
    } else {
        next();
    }
}