export default function getValidators(users) {
    return {
        register(user) {
            return Promise.all([
                users.getOne({
                    username: user.username,
                    active: true
                }),
                users.getOne({
                    email: user.email,
                    active: true
                })
            ]).then(results => {
                const errors = {};

                // username search
                if (results[0]) {
                    errors.username = 'This username is in use'
                }

                // email search
                if (results[1]) {
                    errors.email = 'This email address is in use'
                }

                if (errors.username || errors.email) {
                    throw errors;
                }
            });
        }
    }
}
;