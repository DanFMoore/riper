import axios from "axios";

/**
 * Validate a resource by posting / putting to the required end point via AJAX and then returning back any errors.
 *
 * @param {String} endpoint
 * @param {Boolean} put
 * @returns {Function}
 */
export default function validateOnClient(endpoint, put = false) {
    return function(resource) {
        return axios[put ? 'put' : 'post'](endpoint, resource, {
            headers: {
                'Validate-Only': 1
            }
        }).then(() => {
            return {};
        }).catch(e => {
            return e.response.data;
        });
    }
}