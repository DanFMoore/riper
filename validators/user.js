import emailValidator from "email-validator";
import countries from "../resources/countries.json";

export function register(user) {
    const errors = {};

    ['username', 'email', 'password', 'confirmPassword'].forEach(field => {
        if (!user[field]) {
            errors[field] = 'Required';
        }
    });

    if (user.email && !emailValidator.validate(user.email)) {
        errors.email = 'Invalid email address';
    }

    if (user.confirmPassword && user.confirmPassword !== user.password) {
        errors.confirmPassword = 'Incorrect password';
    }

    return errors;
}

export function login(user) {
    const errors = {};

    if (!user.username) {
        errors.username = 'Required';
    }

    if (!user.password) {
        errors.password = 'Required';
    }

    return errors;
}

/**
 * Validate required fields and country value
 *
 * @param {Object} values
 * @returns {Object}
 */
export function address(values) {
    const errors = {};

    ['name', 'address1', 'town',  'country', 'postcode'].forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required';
        }
    });

    if (values.country && !countries[values.country]) {
        errors.country = 'Invalid value';
    }

    return errors;
}