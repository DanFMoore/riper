import bodyParser from 'body-parser';
import loadAuthenticatedUser from "./auth/loadAuthenticatedUser";
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from "morgan";
import { storeBody } from './actions/server/request';
const settings = require(process.cwd() + '/resources/settings');

/**
 * Add the common functionality to the api application to facilitate redux
 *
 * @param {Object} app express application
 * @param {Function} reduce
 * @param {Object} userActions
 */
export default function(app, reduce, userActions) {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(logger(settings.logging));

    /**
     * Set up CORS and the redux store
     */
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");

        res.header(
            "Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept, Validate-Only, Authorization, Record-Page-No, Record-Per-Page, Record-Order"
        );

        res.header(
            "Access-Control-Expose-Headers",
            "Location, Record-Pagination"
        );

        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

        res.locals.store = createStore(reduce, {}, applyMiddleware(thunk));
        next();
    });

    // Stupid browsers. They always try to request this, so to stop it fucking stuff up, ignore it
    app.get('/favicon.ico', (req, res) => {
        res.sendStatus(404);
    });

    app.use((req, res, next) => {
        if (req.body) {
            res.locals.store.dispatch(storeBody(req.body));
        }

        next();
    });

    // Load auth'd user if logged and then forward all other requests to the default repository end points
    app.use(loadAuthenticatedUser(userActions));

    app.use(function(err, req, res, next) {
        console.error(err);
        res.status(err.status || 500);

        res.send({
            message: err.message,
            error: settings.env === 'development' ? String(err) : null
        });
    });
}
