import Password from "../auth/password";
import faker from "faker";

module.exports = function (users) {
    return function () {
        return Password('testing123').hash().then(hash => {
            const insertions = [];

            for (let i = 1; i <= 10; i++) {
                insertions.push(users.insert({
                    username: faker.internet.userName().toLowerCase(),
                    email: faker.internet.email().toLowerCase(),
                    name: faker.name.findName(),
                    joinDate: faker.date.past(),
                    password: hash,
                    active: true,
                    address1: faker.address.secondaryAddress(),
                    address2: faker.address.streetName(),
                    town: faker.address.city(),
                    county: faker.address.county(),
                    country: faker.address.countryCode(),
                    postcode: faker.address.zipCode()
                }));
            }

            return Promise.all(insertions);
        });
    };
};