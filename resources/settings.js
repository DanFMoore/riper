const dotenv = require("dotenv");

const env = process.env;
dotenv.config({path: env.NODE_ENV === 'test' ? '.test.env' : '.env'});

module.exports = {
    "env": env.NODE_ENV,
    "port": env.PORT,
    "apiPort": env.API_PORT,
    "domain": env.DOMAIN,
    "apiDomain": env.API_DOMAIN,
    "jwtSecret": env.JWT_SECRET,
    "siteName": "[site name]",
    "recordsPerPage": 20,
    "db": {
        "host": env.DB_HOST,
        "port": env.DB_PORT,
        "database": env.DB_NAME,
        "user": env.DB_USER,
        "password": env.DB_PASS
    },
    "countries": require('./countries.json'),
    "logging": env.NODE_ENV === 'development' ? 'dev' : 'common',
    "viewCache": env.NODE_ENV !== 'development',
    "cookie": {
        "secret": env.COOKIE_SECRET,
        "secure": env.NODE_ENV !== 'development'
    }
};