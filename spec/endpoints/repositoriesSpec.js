import endpoints from "../../endpoints/repositories";
import sinon from "sinon";
import {expect} from "chai";

describe('repository endpoints', function() {
    var callbacks, router, res, next, state, recordActions;

    beforeEach(function () {
        // Store the actual functions that are run for each endpoint
        callbacks = {
            get: {},
            post: {},
            put: {},
            delete: {},
            use: []
        };

        router = {
            get: sinon.stub().callsFake((pattern, callback) => {
                callbacks.get[pattern] = callback;
            }),
            post: sinon.stub().callsFake((pattern, callback) => {
                callbacks.post[pattern] = callback;
            }),
            put: sinon.stub().callsFake((pattern, callback) => {
                callbacks.put[pattern] = callback;
            }),
            delete: sinon.stub().callsFake((pattern, callback) => {
                callbacks.delete[pattern] = callback;
            }),
            use: sinon.stub().callsFake((callback) => {
                callbacks.use.push(callback);
            }),
        };

        state = {
            common: {},
            repositoryName: {},
            request: {
                body: {
                    name: 'Jim'
                },
                errors: {},
                invalid: false
            }
        };

        res = {
            sendStatus: sinon.spy(),
            set: sinon.spy(),
            json: sinon.spy(),
            status: sinon.spy(),
            locals: {
                store: {
                    dispatch: sinon.stub().returns(Promise.resolve()),
                    getState: sinon.stub().returns(state)
                }
            }
        };

        recordActions = {};

        next = sinon.stub();
        endpoints(router, recordActions);
    });

    describe('GET :/repository', function () {
        beforeEach(() => {
            recordActions. getRecords = sinon.stub().returns('getRecordsReturnValue');
        });

        describe('with an existent repository', () => {
            beforeEach((done) => {
                state.common.notFound = false;
                state.repositoryName.records = 'repositoryNameRecords';

                state.repositoryName.pagination = {
                    pageNo: 2,
                    perPage: 3
                };

                callbacks.get['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    query: {
                        name: 'bob'
                    },
                    headers: {
                        'record-page-no': '',
                        'record-per-page': ''
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls getRecords', () => {
                sinon.assert.calledWith(recordActions.getRecords, 'repositoryName', {
                    name: 'bob'
                }, {}, undefined, undefined);
            });

            it('calls store.dispatch', () => {
                sinon.assert.calledWith(res.locals.store.dispatch, 'getRecordsReturnValue');
            });

            it('outputs the repositories as json', () => {
                sinon.assert.calledWith(res.json, 'repositoryNameRecords');
            });

            it('sets the pagination header', () => {
                sinon.assert.calledWith(res.set, 'record-pagination', JSON.stringify({
                    pageNo: 2,
                    perPage: 3
                }));
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with a record-order header', () => {
            beforeEach((done) => {
                state.common.notFound = false;
                state.repositoryName.records = 'repositoryNameRecords';

                callbacks.get['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    query: {},
                    headers: {
                        "record-order": JSON.stringify({
                            salary: true
                        })
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls getRecords', () => {
                sinon.assert.calledWith(recordActions.getRecords, 'repositoryName', {}, {
                    salary: true
                }, undefined, undefined);
            });
        });

        describe('with pagination headers', () => {
            beforeEach((done) => {
                state.common.notFound = false;
                state.repositoryName.records = 'repositoryNameRecords';

                callbacks.get['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    query: {},
                    headers: {
                        "record-page-no": 3,
                        "record-per-page": 5
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls getRecords', () => {
                sinon.assert.calledWith(recordActions.getRecords, 'repositoryName', {}, {}, 3, 5);
            });
        });

        describe('with a non-existent repository', () => {
            beforeEach((done) => {
                state.common.notFound = true;

                callbacks.get['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('sends 404', () => {
                sinon.assert.calledWith(res.sendStatus, 404);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with an error', () => {
            beforeEach((done) => {
                res.locals.store.dispatch.returns(Promise.reject('an error'));

                callbacks.get['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls next', () => {
                sinon.assert.calledWith(next, 'an error');
            });

            it('does not output the repositories as json', () => {
                sinon.assert.notCalled(res.json);
            });
        });
    });

    describe('GET :/repository/:id', function () {
        beforeEach(() => {
            recordActions.getRecord = sinon.stub().returns('getRecordReturnValue');
        });

        describe('with an existent record', () => {
            beforeEach((done) => {
                state.common.notFound = false;

                state.repositoryName = {
                    record: 'repositoryNameRecord'
                };

                callbacks.get['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 20
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls getRecord', () => {
                sinon.assert.calledWith(recordActions.getRecord, 'repositoryName', 20);
            });

            it('calls store.dispatch', () => {
                sinon.assert.calledWith(res.locals.store.dispatch, 'getRecordReturnValue');
            });

            it('outputs the records as json', () => {
                sinon.assert.calledWith(res.json, 'repositoryNameRecord');
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with a non-existent record', () => {
            beforeEach((done) => {
                state.common.notFound = true;

                callbacks.get['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 404
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('sends 404', () => {
                sinon.assert.calledWith(res.sendStatus, 404);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with an error', () => {
            beforeEach((done) => {
                res.locals.store.dispatch.returns(Promise.reject('an error'));

                callbacks.get['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 404
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls next', () => {
                sinon.assert.calledWith(next, 'an error');
            });

            it('does not output the record as json', () => {
                sinon.assert.notCalled(res.json);
            });
        });
    });

    describe('POST :/repository', function () {
        beforeEach(() => {
            recordActions.createRecord = sinon.stub().returns('createRecordReturnValue');
            state.common.notFound = false;

            state.repositoryName.createdRecord = {
                id: 5
            };
        });

        describe('with an existent repository', () => {
            beforeEach((done) => {
                callbacks.post['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls createRecord', () => {
                sinon.assert.calledWith(recordActions.createRecord, 'repositoryName', {
                    name: 'Jim'
                });
            });

            it('calls store.dispatch', () => {
                sinon.assert.calledWith(res.locals.store.dispatch, 'createRecordReturnValue');
            });

            it('sets a location header to the created record', () => {
                sinon.assert.calledWith(res.set, 'Location', '/repositoryName/5');
            });

            it('sends a 201', () => {
                sinon.assert.calledWith(res.sendStatus, 201);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with a validate-only header', () => {
            beforeEach((done) => {
                callbacks.post['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    headers: {
                        'validate-only': 1
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('does not calls createRecord', () => {
                sinon.assert.notCalled(recordActions.createRecord);
            });

            it('calls store.dispatch', () => {
                sinon.assert.notCalled(res.locals.store.dispatch);
            });

            it('sends a 200', () => {
                sinon.assert.calledWith(res.sendStatus, 200);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with validation errors', () => {
            beforeEach(() => {
                state.request.invalid = true;

                state.request.errors = {
                    name: 'required'
                };

                callbacks.post['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    headers: {}
                }, res, next);
            });

            it('sends a 422 status', () => {
                sinon.assert.calledWith(res.status, 422);
            });

            it('outputs the errors as json', () => {
                sinon.assert.calledWith(res.json, {
                    name: 'required'
                });
            });

            it('does not call dispatch', () => {
                sinon.assert.notCalled(res.locals.store.dispatch);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with a non-existent repository', () => {
            beforeEach((done) => {
                state.common.notFound = true;

                callbacks.post['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('sends 404', () => {
                sinon.assert.calledWith(res.sendStatus, 404);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with an error', () => {
            beforeEach((done) => {
                res.locals.store.dispatch.returns(Promise.reject('an error'));

                callbacks.post['/:repository']({
                    params: {
                        repository: 'repositoryName'
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls next', () => {
                sinon.assert.calledWith(next, 'an error');
            });

            it('does not send a status', () => {
                sinon.assert.notCalled(res.set);
                sinon.assert.notCalled(res.sendStatus);
            });
        });
    });

    describe('PUT :/repository/:id', function () {
        beforeEach(() => {
            recordActions.updateRecord = sinon.stub().returns('updateRecordReturnValue');
        });

        describe('with an existent repository', () => {
            beforeEach((done) => {
                callbacks.put['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 6
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls updateRecord', () => {
                sinon.assert.calledWith(recordActions.updateRecord, 'repositoryName', 6, {
                    name: 'Jim'
                });
            });

            it('calls store.dispatch', () => {
                sinon.assert.calledWith(res.locals.store.dispatch, 'updateRecordReturnValue');
            });

            it('sends 204', () => {
                sinon.assert.calledWith(res.sendStatus, 204);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with a validate-only header', () => {
            beforeEach((done) => {
                callbacks.put['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 6
                    },
                    headers: {
                        'validate-only': 1
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('does not calls createRecord', () => {
                sinon.assert.notCalled(recordActions.updateRecord);
            });

            it('calls store.dispatch', () => {
                sinon.assert.notCalled(res.locals.store.dispatch);
            });

            it('sends a 200', () => {
                sinon.assert.calledWith(res.sendStatus, 200);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with validation errors', () => {
            beforeEach(() => {
                state.request.invalid = true;

                state.request.errors = {
                    name: 'required'
                };

                callbacks.put['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 6
                    },
                    headers: {}
                }, res, next);
            });

            it('sends a 422 status', () => {
                sinon.assert.calledWith(res.status, 422);
            });

            it('outputs the errors as json', () => {
                sinon.assert.calledWith(res.json, {
                    name: 'required'
                });
            });

            it('does not call dispatch', () => {
                sinon.assert.notCalled(res.locals.store.dispatch);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with a non-existent record', () => {
            beforeEach((done) => {
                state.common.notFound = true;

                callbacks.put['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 6
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('sends 404', () => {
                sinon.assert.calledWith(res.sendStatus, 404);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with an error', () => {
            beforeEach((done) => {
                res.locals.store.dispatch.returns(Promise.reject('an error'));

                callbacks.put['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 6
                    },
                    headers: {}
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls next', () => {
                sinon.assert.calledWith(next, 'an error');
            });

            it('does not send the response', () => {
                sinon.assert.notCalled(res.sendStatus);
            });
        });
    });

    describe('DELETE :/repository/:id', function () {
        beforeEach(() => {
            recordActions.deleteRecord = sinon.stub().returns('deleteRecordReturnValue');
        });

        describe('with an existent repository', () => {
            beforeEach((done) => {
                callbacks.delete['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 7
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls deleteRecord', () => {
                sinon.assert.calledWith(recordActions.deleteRecord, 'repositoryName', 7);
            });

            it('calls store.dispatch', () => {
                sinon.assert.calledWith(res.locals.store.dispatch, 'deleteRecordReturnValue');
            });

            it('sends 204', () => {
                sinon.assert.calledWith(res.sendStatus, 204);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with a non-existent record', () => {
            beforeEach((done) => {
                state.common.notFound = true;

                callbacks.delete['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 7
                    },
                }, res, next);

                setTimeout(done, 0);
            });

            it('sends 404', () => {
                sinon.assert.calledWith(res.sendStatus, 404);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('with an error', () => {
            beforeEach((done) => {
                res.locals.store.dispatch.returns(Promise.reject('an error'));

                callbacks.delete['/:repository/:id']({
                    params: {
                        repository: 'repositoryName',
                        id: 7
                    }
                }, res, next);

                setTimeout(done, 0);
            });

            it('calls next', () => {
                sinon.assert.calledWith(next, 'an error');
            });

            it('does not send the response', () => {
                sinon.assert.notCalled(res.sendStatus);
            });
        });
    });
});