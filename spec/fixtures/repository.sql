CREATE TABLE empty_test (
    id integer NOT NULL,
    "firstName" character varying(255) NOT NULL,
    salary bigint,
    employed boolean DEFAULT false NOT NULL,
    "phoneNumbers" character varying(255)[],
    joined timestamp without time zone NOT NULL
);

CREATE SEQUENCE empty_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE empty_test_id_seq OWNED BY empty_test.id;

ALTER TABLE ONLY empty_test
    ADD CONSTRAINT empty_test_primary PRIMARY KEY (id);

ALTER TABLE ONLY empty_test ALTER COLUMN id SET DEFAULT nextval('empty_test_id_seq'::regclass);


CREATE TABLE populated_test (
    id integer NOT NULL,
    "firstName" character varying(255),
    salary bigint,
    employed boolean DEFAULT false NOT NULL,
    "phoneNumbers" character varying(255)[],
    joined timestamp without time zone NOT NULL
);

CREATE SEQUENCE populated_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE populated_test_id_seq OWNED BY populated_test.id;

ALTER TABLE ONLY populated_test
    ADD CONSTRAINT populated_test_primary PRIMARY KEY (id);

ALTER TABLE ONLY populated_test ALTER COLUMN id SET DEFAULT nextval('populated_test_id_seq'::regclass);

INSERT INTO populated_test ("firstName", salary, employed, "phoneNumbers", joined)
VALUES
('Jim', 25000, true, '{"07771 73391", "655222"}', '2016-01-01 00:00:00'),
('Sally', 35000, false, '{"07741 076338", "255366"}', '2016-01-02 07:35:00'),
('Steve', 40000, false, '{"07711 076338", "255666"}', '2016-01-03 07:35:00');

CREATE TABLE order_test (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    salary int
);

CREATE SEQUENCE order_test_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER SEQUENCE order_test_id_seq OWNED BY order_test.id;

ALTER TABLE ONLY order_test
    ADD CONSTRAINT order_test_primary PRIMARY KEY (id);

ALTER TABLE ONLY order_test ALTER COLUMN id SET DEFAULT nextval('order_test_id_seq'::regclass);

INSERT INTO order_test (id, name, salary)
VALUES
    (2, 'Bob', 25000),
    (3, 'Henry', 30000),
    (1, 'Geoff', 30000);

CREATE TABLE page_test (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);

CREATE SEQUENCE page_test_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER SEQUENCE page_test_id_seq OWNED BY page_test.id;

ALTER TABLE ONLY page_test
    ADD CONSTRAINT page_test_primary PRIMARY KEY (id);

ALTER TABLE ONLY page_test ALTER COLUMN id SET DEFAULT nextval('page_test_id_seq'::regclass);

INSERT INTO page_test (name)
VALUES
    ('Bob'),
    ('Henry'),
    ('Geoff'),
    ('Jim'),
    ('John'),
    ('Dave'),
    ('Geoff');
