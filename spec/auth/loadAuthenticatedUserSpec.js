import getLoadAuthenticatedUser from "../../auth/loadAuthenticatedUser";
import {expect} from "chai";
import sinon from "sinon";

describe('loadAuthenticatedUser', () => {
    var req, res, next, state, loadAuthenticatedUser, actions;

    beforeEach(() => {
        next = sinon.spy();

        actions = {
            getUserByToken: sinon.stub().returns('getUserByTokenReturn')
        };

        req = {
            headers: {}
        };

        state = {
            users: {}
        };

        res = {
            locals: {
                store: {
                    dispatch: sinon.stub().returns(Promise.resolve()),
                    getState: () => state
                }
            },
            sendStatus: sinon.spy()
        };

        loadAuthenticatedUser = getLoadAuthenticatedUser(actions);
    });

    describe('with auth headers', () => {
        beforeEach(() => {
            req.headers.authorization = 'auth';
        });

        it('calls getUserByToken', () => {
            loadAuthenticatedUser(req, res, next);
            sinon.assert.calledWith(actions.getUserByToken, 'auth');
        });

        it('dispatches getUserByToken', () => {
            loadAuthenticatedUser(req, res, next);
            sinon.assert.calledWith(res.locals.store.dispatch, 'getUserByTokenReturn');
        });

        it('does not call next straight away', () => {
            loadAuthenticatedUser(req, res, next);
            sinon.assert.notCalled(next);
        });

        it('catches any error and passes to next asynchronously', done => {
            const error = new Error('an error');
            res.locals.store.dispatch.returns(Promise.reject(error));

            loadAuthenticatedUser(req, res, next);
            sinon.assert.notCalled(next);

            setTimeout(() => {
                sinon.assert.calledWith(next, error);

                done();
            });
        });

        describe('with a logged in user', () => {
            beforeEach(done => {
                state.users.loggedInUser = 'user';
                loadAuthenticatedUser(req, res, next);

                setTimeout(done, 0);
            });

            it('calls next asynchronously', () => {
                sinon.assert.called(next);
            });

            it('does not send a status', () => {
                sinon.assert.notCalled(res.sendStatus);
            })
        });

        describe('without a logged in user', () => {
            beforeEach(done => {
                loadAuthenticatedUser(req, res, next);

                setTimeout(done, 0);
            });

            it('does not call next asynchronously', () => {
                sinon.assert.notCalled(next);
            });

            it('sends a 401 status', () => {
                sinon.assert.calledWith(res.sendStatus, 401);
            })
        });
    });

    describe('without auth headers', () => {
        beforeEach(() => {
            loadAuthenticatedUser(req, res, next);
        });

        it('does not call getUserByToken', () => {
            sinon.assert.notCalled(actions.getUserByToken);
        });

        it('does not dispatch getUserByToken', () => {
            sinon.assert.notCalled(res.locals.store.dispatch);
        });

        it('calls next straight away', () => {
            sinon.assert.called(next);
        });
    });
});