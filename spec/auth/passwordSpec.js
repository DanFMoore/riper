import Password from "../../auth/password";
import {expect} from "chai";

describe('Password', function () {
    var password;
    
    beforeEach(function () {
        password = Password('test-password');
    });

    describe('hash', function () {
        it('generates a string containing the hash and salt', function (done) {
            password.hash().then(function (hash) {
                expect(hash.length).to.equal(438);

                var parts = hash.split('$');
                hash = parts[0];
                var salt = parts[1];

                expect(salt.length).to.equal(93);
                expect(hash.length).to.equal(344);

                done();
            });
        });
    });

    describe('authenticate method', function () {
        it('successfully authenticates when passed the correct hash', function (done) {
            password.hash()
                .then((hash) => {
                    return password.authenticate(hash);
                })
                .then(function (valid) {
                    expect(valid).to.equal(true);

                    done();
                });
        });

        it('fails to authenticate when given an incorrect hash', function (done) {
            var wrongPassword = Password('wrong-password');

            wrongPassword.hash()
                .then((hash) => {
                    return password.authenticate(hash);
                })
                .then(function (valid) {
                    expect(valid).to.equal(false);

                    done();
                });
        });
    });
});
