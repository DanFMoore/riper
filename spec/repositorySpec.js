import {initDb} from "../db";
import Repository from "../repository";
import {expect} from "chai";
import connection from "../resources/connection";
import settings from "../resources/settings";

describe('Repository', function () {
    var repository;
    
    before(function(done) {
        settings.recordsPerPage = 2;

        initDb('./spec/fixtures/repository.sql').then(() => {
            repository = Repository(
                'populated_test',
                ['id', 'firstName', 'salary', 'employed', 'phoneNumbers', 'joined'],
                connection
            );

            done();
        }).catch(done);
    });

    describe('read operations', function () {
        it('should return all records in a promise with getAll()', function(done) {
            var expected = [
                {
                    "id": 1,
                    "firstName": "Jim",
                    "salary": "25000",
                    "employed": true,
                    "phoneNumbers": [
                        "07771 73391",
                        "655222"
                    ],
                    "joined": "2016-01-01 00:00:00"
                },
                {
                    "id": 2,
                    "firstName": "Sally",
                    "salary": "35000",
                    "employed": false,
                    "phoneNumbers": [
                        "07741 076338",
                        "255366"
                    ],
                    "joined": "2016-01-02 07:35:00"
                },
                {
                    "id": 3,
                    "firstName": "Steve",
                    "salary": "40000",
                    "employed": false,
                    "phoneNumbers": [
                        "07711 076338",
                        "255666"
                    ],
                    "joined": "2016-01-03 07:35:00"
                }
            ];

            repository.getAll().then((rows) => {
                expect(JSON.stringify(rows)).to.equal(JSON.stringify(expected));

                done();
            }).catch(done);
        });

        it('should return a single record with getOne()', function (done) {
            var expected = {
                "id": 2,
                "firstName": "Sally",
                "salary": "35000",
                "employed": false,
                "phoneNumbers": [
                    "07741 076338",
                    "255366"
                ],
                "joined": "2016-01-02 07:35:00"
            };

            repository.getOne({ firstName: "Sally" }).then((row) => {
                expect(JSON.stringify(row)).to.equal(JSON.stringify(expected));

                done();
            }).catch(done);
        });

        it('should use the object passed to getAll() for WHERE parameters', function(done) {
            var expected = [
                {
                    "id": 1,
                    "firstName": "Jim",
                    "salary": "25000",
                    "employed": true,
                    "phoneNumbers": [
                        "07771 73391",
                        "655222"
                    ],
                    "joined": "2016-01-01 00:00:00"
                }
            ];

            repository.getAll({
                "firstName": "Jim",
                "employed": true
            }).then((rows) => {
                expect(JSON.stringify(rows)).to.equal(JSON.stringify(expected));

                // This should return no rows
                return repository.getAll({
                    "firstName": "Sally",
                    "employed": true
                });
            }).then((rows) => {
                expect(rows.length).to.equal(0);

                done();
            }).catch(done);
        });

        it('should use the array passed to getAll() for multiple WHERE parameters', done => {
            var expected = [
                {
                    "id": 1,
                    "firstName": "Jim",
                    "salary": "25000",
                    "employed": true,
                    "phoneNumbers": [
                        "07771 73391",
                        "655222"
                    ],
                    "joined": "2016-01-01 00:00:00"
                }
            ];

            repository.getAll([
                {
                    "firstName": "Jim"
                },
                {
                    "employed": true
                }
            ]).then((rows) => {
                expect(JSON.stringify(rows)).to.equal(JSON.stringify(expected));

                // This should return no rows
                return repository.getAll([
                    {
                        "firstName": "Sally"
                    },
                    {
                        "employed": true
                    }
                ]);
            }).then((rows) => {
                expect(rows.length).to.equal(0);

                done();
            }).catch(done);
        });

        it('should construct an IN query when an array is passed as a value', done => {
            repository.getAll({
                "firstName": ["Jim", "Steve"]
            }).then((rows) => {
                expect(rows.length).to.equal(2);

                expect(rows[0].firstName).to.equal('Jim');
                expect(rows[1].firstName).to.equal('Steve');

                done();
            });
        });

        it('should ignore any parameter that is undefined', done => {
            repository.getAll({
                "firstName": ["Jim", "Steve"],
                "employed": undefined
            }).then((rows) => {
                expect(rows.length).to.equal(2);

                expect(rows[0].firstName).to.equal('Jim');
                expect(rows[1].firstName).to.equal('Steve');

                done();
            }).catch(done);
        });

        it('should return nothing if the array for an IN query is empty', done => {
            repository.getAll({
                "firstName": []
            }).then((rows) => {
                expect(rows.length).to.equal(0);

                done();
            }).catch(done);
        });

        it('should return an empty array when there are no records with getAll()', function(done) {
            var repository = Repository(
                'empty_test',
                ['id', 'firstName', 'salary', 'employed', 'phoneNumbers', 'joined'],
                connection
            );

            repository.getAll().then((rows) => {
                expect(rows.length).to.equal(0);
                done();
            });
        });

        it('should return a single record in a promise with getById()', function(done) {
            var expected = {
                "id": 2,
                "firstName": "Sally",
                "salary": "35000",
                "employed": false,
                "phoneNumbers": [
                    "07741 076338",
                    "255366"
                ],
                "joined": "2016-01-02 07:35:00"
            };

            repository.getById(2).then((row) => {
                expect(JSON.stringify(row)).to.equal(JSON.stringify(expected));
                done();
            })
            .catch(done);
        });

        it('should return null when there is no matching record with getById()', function(done) {
            repository.getById(4).then((row) => {
                expect(row).to.equal(null);
                done();
            }).catch(console.error);
        });

        describe('ordering', () => {
            var orderRepo;

            beforeEach(() => {
                orderRepo = Repository('order_test', ['id', 'name', 'salary'], connection);
            });

            it('orders by id by default', done => {
                orderRepo.getAll().then(rows => {
                    expect(rows[0].id).to.equal(1);
                    expect(rows[1].id).to.equal(2);
                    expect(rows[2].id).to.equal(3);

                    done();
                }).catch(done);
            });

            it('orders by the column specified', done => {
                orderRepo.getAll(undefined, {
                    name: true
                }).then(rows => {
                    expect(rows[0].name).to.equal('Bob');
                    expect(rows[1].name).to.equal('Geoff');
                    expect(rows[2].name).to.equal('Henry');

                    done();
                }).catch(done);
            });

            it('orders by the id if the first order column values are the same', done => {
                orderRepo.getAll(undefined, {
                    salary: true
                }).then(rows => {
                    expect(rows[1].id).to.equal(1);
                    expect(rows[2].id).to.equal(3);

                    done();
                }).catch(done);
            });

            it('orders by desc if the object value is false', done => {
                orderRepo.getAll(undefined, {
                    name: false
                }).then(rows => {
                    expect(rows[0].name).to.equal('Henry');
                    expect(rows[1].name).to.equal('Geoff');
                    expect(rows[2].name).to.equal('Bob');

                    done();
                }).catch(done);
            });

            it('orders by multiple columns', done => {
                orderRepo.getAll(undefined, {
                    salary: true,
                    name: false
                }).then(rows => {
                    expect(rows[0].salary).to.equal(25000);
                    expect(rows[1].salary).to.equal(30000);
                    expect(rows[1].name).to.equal('Henry');
                    expect(rows[2].salary).to.equal(30000);
                    expect(rows[2].name).to.equal('Geoff');

                    done();
                }).catch(done);
            });

            it('throws an exception if an incorrect column is used', done => {
                orderRepo.getAll(undefined, {
                    gggg: true
                }).then(() => {
                    done(new Error('should throw error'));
                }).catch(e => {
                    expect(e.message).to.equal('Incorrect order columns specified');

                    done();
                });
            });

            it('orders from getOne', done => {
                orderRepo.getOne(undefined, {
                    salary: false
                }).then(row => {
                    expect(row.name).to.equal('Geoff');

                    done();
                }).catch(done);
            });
        });

        describe('pagination', () => {
            var paginationRepo;

            beforeEach(() => {
                paginationRepo = Repository('page_test', ['id', 'name'], connection);
            });

            it('limits the records to the first page', done => {
                paginationRepo.getAll(undefined, undefined, 1).then(rows => {
                    expect(rows.length).to.equal(2);
                    expect(rows[0].id).to.equal(1);
                    expect(rows[1].id).to.equal(2);

                    done();
                }).catch(done);
            });

            it('limits the records to the second page', done => {
                paginationRepo.getAll(undefined, undefined, 2).then(rows => {
                    expect(rows.length).to.equal(2);
                    expect(rows[0].id).to.equal(3);
                    expect(rows[1].id).to.equal(4);

                    done();
                }).catch(done);
            });

            it('limits the records to the last page', done => {
                paginationRepo.getAll(undefined, undefined, 4).then(rows => {
                    expect(rows.length).to.equal(1);
                    expect(rows[0].id).to.equal(7);

                    done();
                }).catch(done);
            });

            it('limits the records to a non-default perPage number', done => {
                paginationRepo.getAll(undefined, undefined, 2, 3).then(rows => {
                    expect(rows.length).to.equal(3);
                    expect(rows[0].id).to.equal(4);
                    expect(rows[1].id).to.equal(5);
                    expect(rows[2].id).to.equal(6);

                    done();
                }).catch(done);
            });

            it('returns the pagination values', done => {
                paginationRepo.getAll(undefined, undefined, 3).then(rows => {
                    expect(rows.pagination).to.deep.equal({
                        totalRecords: 7,
                        pageNo: 3,
                        perPage: 2,
                        totalPages: 4
                    });

                    done();
                }).catch(done);
            });
        });
    });

    describe('write operations', function () {
        it('should insert a record with insert()', function(done) {
            var newRecord = {
                // The id should be stripped out before insertion
                "id": 9494,
                "firstName": "Richard",
                "salary": "15000",
                "employed": true,
                "phoneNumbers": [
                    "0111 23123",
                    "0222 453453"
                ],
                "joined": "2016-01-03 07:35:00"
            };

            var expected = {
                "id": 4,
                "firstName": "Richard",
                "salary": "15000",
                "employed": true,
                "phoneNumbers": [
                    "0111 23123",
                    "0222 453453"
                ],
                "joined": "2016-01-03 07:35:00"
            };

            repository.insert(newRecord).then((insertId) => {
                expect(insertId).to.equal(4);

                return repository.getById(4);
            }).then((row) => {
                expect(JSON.stringify(row)).to.equal(JSON.stringify(expected));
                done();
            }).catch(done);
        });

        it('should convert empty strings to null with insert()', function(done) {
            var newRecord = {
                // The id should be stripped out before insertion
                "id": 9494,
                "firstName": "",
                "salary": 0,
                "employed": true,
                "phoneNumbers": [
                    "0111 23123",
                    "0222 453453"
                ],
                "joined": "2016-01-03 07:35:00"
            };

            var expected = {
                "id": 5,
                "firstName": null,
                "salary": "0",
                "employed": true,
                "phoneNumbers": [
                    "0111 23123",
                    "0222 453453"
                ],
                "joined": "2016-01-03 07:35:00"
            };

            repository.insert(newRecord).then((insertId) => {
                return repository.getById(insertId);
            }).then((row) => {
                expect(JSON.stringify(row)).to.equal(JSON.stringify(expected));
                done();
            }).catch(done);
        });

        it('should update a record with update()', function(done) {
            var updatedRecord = {
                "id": 2,
                "firstName": "Sally",
                "salary": "36000",
                "employed": true,
                "phoneNumbers": [
                    "0333 024323",
                    "34444",
                    "999"
                ],
                "joined": "2015-01-02 07:35:00"
            };

            repository.update(2, updatedRecord).then((success) => {
                expect(success).to.equal(true);

                return repository.getById(2);
            }).then((row) => {
                expect(JSON.stringify(row)).to.equal(JSON.stringify(updatedRecord));
                done();
            }).catch(done);
        });

        it('should convert an empty string to null with update()', done => {
            var updatedRecord = {
                "id": 2,
                "firstName": "",
                "salary": 0,
                "employed": true,
                "phoneNumbers": [
                    "0333 024323",
                    "34444",
                    "999"
                ],
                "joined": "2015-01-02 07:35:00"
            };

            repository.update(2, updatedRecord).then((success) => {
                expect(success).to.equal(true);

                return repository.getById(2);
            }).then((row) => {
                expect(row.firstName).to.equal(null);
                expect(row.salary).to.equal('0');
                done();
            }).catch(done);
        });

        it('should return false when updating a record that doesn\'t exist with update()', function(done) {
            var updatedRecord = {
                "id": 6,
                "firstName": "Sally",
                "salary": "36000",
                "employed": true,
                "phoneNumbers": [
                    "0333 024323",
                    "34444",
                    "999"
                ],
                "joined": new Date("2015-01-02T07:35:00")
            };

            repository.update(6, updatedRecord).then((success) => {
                expect(success).to.equal(false);
                done();
            });
        });

        it('should delete a record with delete()', function(done) {
            repository.delete(2).then((success) => {
                expect(success).to.equal(true);

                return repository.getById(2);
            }).then((row) => {
                expect(row).to.equal(null);
                done();
            }).catch(done);
        });

        it('should return false when deleting a record that doesn\'t exist with delete()', function(done) {
            repository.delete(10).then((success) => {
                expect(success).to.equal(false);
                done();
            }).catch(done);
        });
    });

    describe('errors', function () {
        var errorRepo;

        beforeEach(function () {
            // Easiest way to force errors is to specify incorrect table
            errorRepo = Repository(
                'doesnt_exist',
                ['id', 'incorrect', 'stupidField', 'thisIsNotThere'],
                connection
            );
        });

        it('should throw errors through the promise interface from getAll()', function(done) {
            errorRepo.getAll().catch((e) => {
                expect(e.name).to.equal('error');
                done();
            });
        });

        it('should throw errors through the promise interface from getById()', function(done) {
            errorRepo.getById(1).catch((e) => {
                expect(e.name).to.equal('error');
                done();
            });
        });

        it('should throw errors through the promise interface from update()', function(done) {
            errorRepo.update(1, {"id": 1}).catch((e) => {
                expect(e.name).to.equal('error');
                done();
            });
        });

        it('should throw errors through the promise interface from delete()', function(done) {
            errorRepo.delete(1).catch((e) => {
                expect(e.name).to.equal('error');
                done();
            });
        });
    });
});
