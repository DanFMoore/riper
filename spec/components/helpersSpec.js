import { expect } from "chai";
import { explodeAddress } from "../../components/helpers";

describe('component helpers', () => {
    describe('explodeAddress', () => {
        var address;

        beforeEach(() => {
            address = {
                address1: '12 Nelson Mandela House',
                address2: 'Denbigh Road',
                town: 'Mold',
                county: 'Flintshire',
                country: 'GB',
                postcode: 'CH7 5QG'
            };
        });

        it('returns an array of the details in order with the country converted', () => {
            const result = explodeAddress(address);

            expect(result.length).to.equal(6);
            expect(result[0]).to.equal('12 Nelson Mandela House');
            expect(result[1]).to.equal('Denbigh Road');
            expect(result[2]).to.equal('Mold');
            expect(result[3]).to.equal('Flintshire');
            expect(result[4]).to.equal('CH7 5QG');
            expect(result[5]).to.equal('United Kingdom');
        });

        it('removes any non-filled in details', () => {
            delete address.address2;
            delete address.county;
            delete address.country;

            const result = explodeAddress(address);

            expect(result.length).to.equal(3);
            expect(result[0]).to.equal('12 Nelson Mandela House');
            expect(result[1]).to.equal('Mold');
            expect(result[2]).to.equal('CH7 5QG');
        });
    });
});