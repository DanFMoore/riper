import React from 'react';
import sinon from "sinon";
import { shallow } from "enzyme";
import { expect } from "chai";
import { Register } from "../../../components/forms/register";

describe('Register form', () => {
    var props, component;

    beforeEach(() => {
        props = {
            handleSubmit: sinon.stub(),
            register: sinon.spy(),
            logout: sinon.spy(),
            hideModal: sinon.spy(),
            submitting: false,
            submitted: false,
            actionType: null
        };

        component = shallow(
            <Register {...props} />
        );
    });

    it('calls the register action when form submitted', done => {
        component.setProps({
            handleSubmit: register => {
                register({
                    username: 'bob',
                    password: 'testing123'
                });

                sinon.assert.calledWith(props.register, {
                    username: 'bob',
                    password: 'testing123'
                });

                done();
            }
        });

        component.find('form').simulate('submit');
    });
});