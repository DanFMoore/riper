import React from 'react';
import sinon from "sinon";
import { shallow } from "enzyme";
import { expect } from "chai";
import { Login } from "../../../components/forms/login";

describe('Register form', () => {
    var props, component;

    beforeEach(() => {
        props = {
            handleSubmit: sinon.stub(),
            login: sinon.spy(),
            submitting: false,
            actionType: null
        };

        component = shallow(
            <Login {...props} />
        );
    });

    it('does not initially show an error message', () => {
        expect(component.find('.alert-danger').length).to.equal(0);
    });

    it('outputs the error messsage', () => {
        component.setProps({
            loginFailureMessage: 'you done logged in wrong'
        });

        expect(component.find('.alert-danger').length).to.equal(1);
        expect(component.find('.alert-danger').text()).to.equal('you done logged in wrong');
    });

    it('calls the login action when form submitted', done => {
        component.setProps({
            handleSubmit: login => {
                login({
                    username: 'bob',
                    password: 'testing123'
                });

                sinon.assert.calledWith(props.login, {
                    username: 'bob',
                    password: 'testing123'
                });

                done();
            }
        });

        component.find('form').simulate('submit');
    });

    it('calls the overriden login action when form submitted if specified', done => {
        const overrideLogin = sinon.spy();

        component.setProps({
            overrideLogin,
            handleSubmit: login => {
                login({
                    username: 'bob',
                    password: 'testing123'
                });

                sinon.assert.calledWith(overrideLogin, {
                    username: 'bob',
                    password: 'testing123'
                });

                sinon.assert.notCalled(props.login);

                done();
            }
        });

        component.find('form').simulate('submit');
    });
});