import React from 'react';
import sinon from "sinon";
import { shallow } from "enzyme";
import { expect } from "chai";
import { Register } from "../../../components/user/register";
import * as types from "../../../actions/types";

describe('Register component', () => {
    var props, component;

    beforeEach(() => {
        props = {
            handleSubmit: sinon.stub(),
            register: sinon.spy(),
            logout: sinon.spy(),
            hideModal: sinon.spy(),
            submitting: false,
            submitted: false,
            actionType: null
        };

        component = shallow(
            <Register {...props} />
        );
    });

    it('initially logs out', () => {
        sinon.assert.called(props.logout);
    });

    it('renders a form and not a message about having registered', () => {
        expect(component.find('Connect(ReduxForm)').length).to.equal(1);
        expect(component.find('div.registered').length).to.equal(0);
    });

    it('renders a message about having registered instead of the form after registering', () => {
        component.setProps({
            actionType: types.USER_REGISTER,
        });

        expect(component.find('Connect(ReduxForm)').length).to.equal(0);
        expect(component.find('div.registered').length).to.equal(1);
    });
});