import React from 'react';
import { shallow } from "enzyme";
import { expect } from "chai";
import { TitleListener } from "../../components/titleListener";

describe('Title listener form', () => {
    var props, component;

    beforeEach(() => {
        props = {
            title: 'Original title'
        };

        window.document.title = 'Original html title';

        component = shallow(
            <TitleListener {...props} />
        );
    });

    it('does not change the html title on mounting', () => {
        expect(window.document.title).to.equal('Original html title');
    });

    it('changes the html title when the title is updated', () => {
        component.setProps({
            title: 'New title'
        });

        expect(window.document.title).to.equal('New title');
    });
});