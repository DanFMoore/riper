import * as types from "../../actions/types";
import {expect} from "chai";
import getReducer from "../../reducers/repositories";

describe('repositories reducer', () => {
    var oldState, oldStateEncoded, newState, record, reduce;

    beforeEach(() => {
        oldState = {
            records: [
                {
                    id: 56,
                    name: 'Jim',
                    town: 'Flint'
                },
                {
                    id: 57,
                    name: 'Bob',
                    town: 'Mold'
                }
            ],
            createdRecord: {},
            updatedRecord: {},
            deletedRecord: {},
            record: {
                id: 58,
                name: 'Jeff',
                town: 'Chester'
            }
        };

        oldStateEncoded = JSON.stringify(oldState);
        reduce = getReducer('repositoryName');
    });

    describe(types.REPOSITORY_GET_RECORDS, () => {
        var records;

        beforeEach(() => {
            records = [
                {
                    id: 58,
                    name: 'Geoff'
                },
                {
                    id: 59,
                    name: 'Billy'
                }
            ];

            newState = reduce(oldState, {
                type: types.REPOSITORY_GET_RECORDS,
                repository: 'repositoryName',
                records,
                pagination: {
                    perPage: 20,
                    pageNo: "3"
                }
            });
        });

        it('populates the records as state.records', () => {
            expect(newState.records).to.equal(records);
        });

        it('populates state.pagination', () => {
            expect(newState.pagination).to.deep.equal({
                perPage: 20,
                pageNo: 3
            });
        });

        it('does nothing if you pass a different repository name', () => {
            newState = reduce(oldState, {
                type: types.REPOSITORY_GET_RECORDS,
                repository: 'otherRepository',
                records
            });

            expect(newState).to.equal(oldState);
        });

        commonTests(['createdRecord', 'updatedRecord', 'deletedRecord', 'record']);
    });

    describe(types.REPOSITORY_GET_RECORD, () => {
        beforeEach(() => {
            record = {
                id: 58,
                name: 'Geoff'
            };

            newState = reduce(oldState, {
                type: types.REPOSITORY_GET_RECORD,
                repository: 'repositoryName',
                record
            });
        });

        it('populates the record into state.record', () => {
            expect(newState.record).to.equal(record);
        });

        commonTests(['createdRecord', 'updatedRecord', 'deletedRecord', 'records']);
    });

    describe(types.REPOSITORY_CREATE_RECORD, () => {
        beforeEach(() => {
            record = {
                name: 'Joe'
            };

            newState = reduce(oldState, {
                type: types.REPOSITORY_CREATE_RECORD,
                repository: 'repositoryName',
                record,
                id: 60
            });
        });

        it('populates the created record as state.createdRecord with the id', () => {
            expect(newState.createdRecord).to.deep.equal({
                name: 'Joe',
                id: 60
            });
        });

        commonTests(['records', 'record', 'updatedRecord', 'deletedRecord']);
    });

    describe(types.REPOSITORY_UPDATE_RECORD, () => {
        beforeEach(() => {
            record = {
                name: 'Dan'
            };

            newState = reduce(oldState, {
                type: types.REPOSITORY_UPDATE_RECORD,
                repository: 'repositoryName',
                record,
                id: 57
            });
        });

        it('populates the record as state.updatedRecord', () => {
            expect(newState.updatedRecord).to.deep.equal({
                name: 'Dan',
                id: 57
            });
        });

        it('updates the respective record in state.records', () => {
            expect(newState.records[1]).to.deep.equal({
                id: 57,
                name: 'Dan',
                town: 'Mold'
            });
        });

        describe('state.record', () => {
            it('updates state.record if it has the same id', () => {
                newState = reduce(oldState, {
                    type: types.REPOSITORY_UPDATE_RECORD,
                    repository: 'repositoryName',
                    record,
                    id: 58
                });

                expect(newState.record).to.deep.equal({
                    id: 58,
                    name: 'Dan',
                    town: 'Chester'
                });
            });

            it('does not update state.record if state.record is undefined', () => {
                oldState.record = undefined;

                newState = reduce(oldState, {
                    type: types.REPOSITORY_UPDATE_RECORD,
                    repository: 'repositoryName',
                    record,
                    id: 58
                });

                expect(newState.record).to.equal(undefined);
            });
        });

        commonTests(['createdRecord', 'deletedRecord', 'record']);
    });

    describe(types.REPOSITORY_DELETE_RECORD, () => {
        beforeEach(() => {
            record = {
                name: 'Bob'
            };

            newState = reduce(oldState, {
                type: types.REPOSITORY_DELETE_RECORD,
                repository: 'repositoryName',
                record,
                id: 57
            });
        });

        it('populates the record as state.deletedRecord', () => {
            expect(newState.deletedRecord).to.deep.equal({
                id: 57,
                name: 'Bob'
            });
        });

        it('deletes the respective record from state.records', () => {
            expect(newState.records.length).to.equal(1);
            expect(newState.records[0]).to.equal(oldState.records[0]);
        });

        describe('with no record in the action', () => {
            beforeEach(() => {
                newState = reduce(oldState, {
                    type: types.REPOSITORY_DELETE_RECORD,
                    repository: 'repositoryName',
                    id: 56
                });
            });

            it('populates the record as state.deletedRecord', () => {
                expect(newState.deletedRecord).to.deep.equal({
                    id: 56
                });
            });

            it('deletes the respective record from state.records', () => {
                expect(newState.records.length).to.equal(1);
                expect(newState.records[0]).to.equal(oldState.records[1]);
            });
        });

        describe('state.record', () => {
            it('deletes state.record if it has the same id', () => {
                newState = reduce(oldState, {
                    type: types.REPOSITORY_DELETE_RECORD,
                    repository: 'repositoryName',
                    id: 58
                });

                expect(newState.record).to.equal(undefined);
            });

            it('does not do anything to state.record if state.record is undefined', () => {
                oldState.record = undefined;

                newState = reduce(oldState, {
                    type: types.REPOSITORY_DELETE_RECORD,
                    repository: 'repositoryName',
                    id: 58
                });

                expect(newState.record).to.equal(undefined);
            });
        });

        commonTests(['createdRecord', 'updatedRecord', 'record']);
    });

    function commonTests(otherFields) {
        it('does not affect the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });

        it('leaves the other fields alone', () => {
            otherFields.forEach(field => {
                expect(newState[field]).to.equal(oldState[field]);
            });
        });
    }
});
