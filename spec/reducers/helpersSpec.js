import {expect} from "chai";
import { addRelatedRecords } from "../../reducers/helpers";

describe('reducer helpers', () => {
    describe('addRelatedRecords', () => {
        var oldState, oldStateEncoded, newState, source;

        beforeEach(() => {
            oldState = [
                {
                    id: 1,
                    categoryId: '1' // Annoyingly these can be strings instead of numbers
                },
                {
                    id: 2,
                    categoryId: '2'
                },
                {
                    id: 3,
                    categoryId: '2',
                    category: {
                        thing: 'blah'
                    }
                },
                {
                    id: 4,
                    categoryId: '4'
                },
                {
                    id: 5
                },
                {
                    id: 6,
                    categoryId: '4',
                    category: 'my category'
                }
            ];

            oldStateEncoded = JSON.stringify(oldState);

            source = [
                {
                    id: 1,
                    name: 'Category 1'
                },
                {
                    id: 2,
                    name: 'Category 2'
                },
                {
                    id: 3,
                    name: 'Category 3'
                }
            ];

            newState = addRelatedRecords(oldState, source, 'category', 'categoryId');
        });

        it('does nothing if the wrong foreignKey is specified', () => {
            newState = addRelatedRecords(oldState, source, 'category', 'publisherId');

            expect(JSON.stringify(newState)).to.equal(oldStateEncoded);
        });

        it('adds related record 1 to record 1', () => {
            expect(newState[0].category).to.equal(source[0]);
        });

        it('adds related record 2 to record 2', () => {
            expect(newState[1].category).to.equal(source[1]);
        });

        it('replaces the related record in record 3', () => {
            expect(newState[2].category).to.equal(source[1]);
        });

        it('does not add a related record to records 4 or 5', () => {
            expect(newState[3].category).to.equal(undefined);
            expect(newState[4].category).to.equal(undefined);
        });

        it('leaves along the related record for record 6', () => {
            expect(newState[5].category).to.equal('my category');
        });

        it('adds the related record as a different field', () => {
            newState = addRelatedRecords(oldState, source, 'publisher', 'categoryId');

            expect(newState[0].publisher).to.equal(source[0]);
        });

        it('adds the related record if the relationships are reversed', () => {
            oldState = [
                {
                    id: 1,
                    category: 'my category'
                },
                {
                    id: 2,
                },
                {
                    id: 3,
                },
                {
                    id: 4,
                },
            ];

            source = [
                {
                    id: 1,
                    name: 'Category 1',
                    productId: '3'
                },
                {
                    id: 2,
                    name: 'Category 2',
                    productId: '2'
                }
            ];

            newState = addRelatedRecords(oldState, source, 'category', 'productId', true);

            expect(newState[0].category).to.equal('my category');
            expect(newState[1].category).to.equal(source[1]);
            expect(newState[2].category).to.equal(source[0]);
            expect(newState[3].category).to.equal(undefined);
        });

        it('adds the related records as an array if the relationships are reversed', () => {
            oldState = [
                {
                    id: 1,
                    categories: ['my category']
                },
                {
                    id: 2,
                },
                {
                    id: 3,
                },
                {
                    id: 4,
                },
            ];

            source = [
                {
                    id: 1,
                    name: 'Category 1',
                    productId: '3'
                },
                {
                    id: 2,
                    name: 'Category 2',
                    productId: '2'
                },
                {
                    id: 3,
                    name: 'Category 3',
                    productId: '3'
                }
            ];

            newState = addRelatedRecords(oldState, source, 'categories', 'productId', true, true);

            expect(newState[0].categories).to.deep.equal([]);
            expect(newState[1].categories).to.deep.equal([source[1]]);
            expect(newState[2].categories).to.deep.equal([source[0], source[2]]);
            expect(newState[3].categories).to.deep.equal([]);
        });

        it('works if state is a single object', () => {
            oldState = oldState[0];
            newState = addRelatedRecords(oldState, source, 'category', 'categoryId');

            expect(newState.category).to.equal(source[0]);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });
});