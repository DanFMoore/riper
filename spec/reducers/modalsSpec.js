import * as types from "../../actions/types";
import {expect} from "chai";
import reduce from "../../reducers/modals";

describe('modals reducer', () => {
    var oldState, oldStateEncoded, newState;

    beforeEach(() => {
        oldState = {
            user: false,
            product: true
        };

        oldStateEncoded = JSON.stringify(oldState);
    });

    describe(types.MODAL_SHOW, () => {
        it('adds a state of true for a non-existent modal', () => {
            newState = reduce(oldState, {
                type: types.MODAL_SHOW,
                modal: 'options'
            });

            expect(newState.options).to.equal(true);
        });

        it('leaves a state of true for a modal that is already true', () => {
            newState = reduce(oldState, {
                type: types.MODAL_SHOW,
                modal: 'product'
            });

            expect(newState.product).to.equal(true);
        });

        it('it turns a false modal into true', () => {
            newState = reduce(oldState, {
                type: types.MODAL_SHOW,
                modal: 'user'
            });

            expect(newState.user).to.equal(true);
        });

        it('disables all other modals', () => {
            newState = reduce(oldState, {
                type: types.MODAL_SHOW,
                modal: 'user'
            });

            expect(newState.product).to.equal(false);
        });

        it('does not touch the old state', () => {
            reduce(oldState, {
                type: types.MODAL_SHOW,
                modal: 'user'
            });

            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.MODAL_HIDE, () => {
        it('adds a state of false for a non-existent modal', () => {
            newState = reduce(oldState, {
                type: types.MODAL_HIDE,
                modal: 'options'
            });

            expect(newState.options).to.equal(false);
        });

        it('leaves a state of false for a modal that is already false', () => {
            newState = reduce(oldState, {
                type: types.MODAL_HIDE,
                modal: 'user'
            });

            expect(newState.user).to.equal(false);
        });

        it('it turns a true modal into false', () => {
            newState = reduce(oldState, {
                type: types.MODAL_HIDE,
                modal: 'product'
            });

            expect(newState.product).to.equal(false);
        });

        it('leaves all other modals alone', () => {
            newState = reduce(oldState, {
                type: types.MODAL_HIDE,
                modal: 'user'
            });

            expect(newState.product).to.equal(true);
        });

        it('does not touch the old state', () => {
            reduce(oldState, {
                type: types.MODAL_HIDE,
                modal: 'user'
            });

            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });
});