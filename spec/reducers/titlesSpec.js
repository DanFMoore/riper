import * as types from "../../actions/types";
import {expect} from "chai";
import getReducer from "../../reducers/titles";

describe('titles reducer', () => {
    var state, reduce;

    beforeEach(() => {
        reduce = getReducer('site name');
    });

    describe('initially', () => {
        it('sets the siteName', () => {
            state = reduce(undefined, {
                type: 'do nothing'
            });

            expect(state.siteName).to.equal('site name');
        });

        it('sets the html title to the site name', () => {
            state = reduce(undefined, {
                type: 'do nothing'
            });

            expect(state.html).to.equal('site name');
        });
    });

    describe(types.TITLE_SET, () => {
        describe('with a html title', () => {
            beforeEach(() => {
                state = reduce(undefined, {
                    type: types.TITLE_SET,
                    title: 'My first title',
                    html: 'First title - site name'
                });
            });

            it('sets the html title', () => {
                expect(state.html).to.equal('First title - site name');
            });

            it('sets the main title', () => {
                expect(state.title).to.equal('My first title');
            });

            it('leaves the site name', () => {
                expect(state.siteName).to.equal('site name');
            });
        });

        describe('without a html title', () => {
            beforeEach(() => {
                state = reduce(undefined, {
                    type: types.TITLE_SET,
                    title: 'My first title'
                });
            });

            it('infers the html title from the main title and site name', () => {
                expect(state.html).to.equal('My first title - site name');
            });

            it('sets the main title', () => {
                expect(state.title).to.equal('My first title');
            });

            it('leaves the site name', () => {
                expect(state.siteName).to.equal('site name');
            });
        });
    });
});