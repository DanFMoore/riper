import * as types from "../../actions/types";
import { expect } from "chai";
import reduce from "../../reducers/request";

describe('request reducer', () => {
    var state;

    describe(types.REQUEST_VALIDATE, () => {
        beforeEach(() => {
            state = {
                // simulate having sync errors already populated
                errors: {
                    name: 'Required',
                    username: 'Required'
                },
                invalid: true
            };
        });

        it('adds the errors to state.errors', () => {
            const newState = reduce(state, {
                type: types.REQUEST_VALIDATE,
                errors: {
                    password: 'Not long enough',
                    email: 'Invalid email address'
                }
            });

            expect(newState.errors).to.deep.equal({
                name: 'Required',
                username: 'Required',
                password: 'Not long enough',
                email: 'Invalid email address'
            });
        });

        it('does not overwrite existing errors', () => {
            const newState = reduce(state, {
                type: types.REQUEST_VALIDATE,
                errors: {
                    name: 'Not long enough',
                    email: 'Invalid email address'
                }
            });

            expect(newState.errors).to.deep.equal({
                name: 'Required',
                username: 'Required',
                email: 'Invalid email address'
            });
        });

        it('leaves invalid as true if no errors are dispatched', () => {
            const newState = reduce(state, {
                type: types.REQUEST_VALIDATE,
                errors: {}
            });

            expect(newState.invalid).to.equal(true);
        });

        it('leaves invalid as false if no errors are dispatched', () => {
            state.invalid = false;

            const newState = reduce(state, {
                type: types.REQUEST_VALIDATE,
                errors: {}
            });

            expect(newState.invalid).to.equal(false);
        });

        it('leaves invalid as false if undefined is dispatched', () => {
            state.invalid = false;

            const newState = reduce(state, {
                type: types.REQUEST_VALIDATE
            });

            expect(newState.invalid).to.equal(false);
        });

        it('sets invalid to true if errors are dispatched', () => {
            state.invalid = false;

            const newState = reduce(state, {
                type: types.REQUEST_VALIDATE,
                errors: {
                    name: 'Not long enough',
                }
            });

            expect(newState.invalid).to.equal(true);
        });

        it('leaves invalid as true if errors are dispatched', () => {
            const newState = reduce(state, {
                type: types.REQUEST_VALIDATE,
                errors: {
                    name: 'Not long enough',
                }
            });

            expect(newState.invalid).to.equal(true);
        });
    });
});