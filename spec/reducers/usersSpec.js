import * as types from "../../actions/types";
import {expect} from "chai";
import reduce from "../../reducers/users";

describe('users reducer', () => {
    var oldState, oldStateEncoded, newState;

    beforeEach(() => {
        oldState = {
            loggedInUser: {
                id: 100,
                name: 'Bob',
                town: 'Mold'
            },
            loginFailureMessage: 'blah',
            token: 'blah'
        };

        oldStateEncoded = JSON.stringify(oldState);
    });

    describe(types.USERNAME_NOT_FOUND, () => {
        beforeEach(() => {
            newState = reduce(oldState, {
                type: types.USERNAME_NOT_FOUND
            });
        });

        it('removes the logged in user', () => {
            expect(newState.loggedInUser).to.equal(null);
        });

        it('adds loginFailureMessage', () => {
            expect(newState.loginFailureMessage).to.equal('Username not found');
        });

        it('adds the type', () => {
            expect(newState.actionType).to.equal(types.USERNAME_NOT_FOUND);
        });

        it('removes the token', () => {
            expect(newState.token).to.equal(null);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.USER_TOKEN_EXPIRED, () => {
        beforeEach(() => {
            newState = reduce(oldState, {
                type: types.USER_TOKEN_EXPIRED
            });
        });

        it('removes the logged in user', () => {
            expect(newState.loggedInUser).to.equal(null);
        });

        it('adds loginFailureMessage', () => {
            expect(newState.loginFailureMessage).to.equal('User session expired');
        });

        it('adds the type', () => {
            expect(newState.actionType).to.equal(types.USER_TOKEN_EXPIRED);
        });

        it('removes the token', () => {
            expect(newState.token).to.equal(null);
        });

        it('does not touch the old state', () => {
            expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
        });
    });

    describe(types.USER_AUTH, () => {
        var user;

        describe('with a successful login', () => {
            beforeEach(() => {
                user = {
                    id: 123,
                    password: 'supersecrethash',
                    name: 'Jim'
                };

                newState = reduce(oldState, {
                    type: types.USER_AUTH,
                    loggedInUser: user,
                    result: true,
                    token: 'eyJh'
                });
            });

            it('adds the logged in user', () => {
                expect(newState.loggedInUser).to.equal(user);
            });

            it('removes the login message', () => {
                expect(newState.loginFailureMessage).to.equal(null);
            });

            it('adds the token', () => {
                expect(newState.token).to.equal('eyJh');
            });

            it('adds the type', () => {
                expect(newState.actionType).to.equal(types.USER_AUTH);
            });

            it('does not touch the old state', () => {
                expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
            });
        });

        describe('with an unsuccessful login', () => {
            beforeEach(() => {
                newState = reduce(oldState, {
                    type: types.USER_AUTH,
                    loggedInUser: null
                });
            });

            it('removes the logged in user', () => {
                expect(newState.loggedInUser).to.equal(null);
            });

            it('adds loginFailureMessage', () => {
                expect(newState.loginFailureMessage).to.equal('Incorrect password');
            });

            it('adds the type', () => {
                expect(newState.actionType).to.equal(types.USER_AUTH);
            });

            it('removes the token', () => {
                expect(newState.token).to.equal(null);
            });

            it('does not touch the old state', () => {
                expect(JSON.stringify(oldState)).to.equal(oldStateEncoded);
            });
        });
    });

    describe(types.REPOSITORY_UPDATE_RECORD, () => {
        it('updates the logged in user if it has the same id', () => {
            newState = reduce(oldState, {
                type: types.REPOSITORY_UPDATE_RECORD,
                repository: 'users',
                id: 100,
                record: {
                    name: 'Jim',
                    id: 102 // This id should be ignored and taken from the action instead
                }
            });

            expect(newState.loggedInUser).to.deep.equal({
                id: 100,
                name: 'Jim',
                town: 'Mold'
            });
        });

        it('does not update the logged in user with a different id', () => {
            newState = reduce(oldState, {
                type: types.REPOSITORY_UPDATE_RECORD,
                repository: 'users',
                id: 101,
                record: {
                    name: 'Jim'
                }
            });

            expect(newState.loggedInUser).to.equal(oldState.loggedInUser);
        });

        it('does not update the logged user with a different repository', () => {
            newState = reduce(oldState, {
                type: types.REPOSITORY_UPDATE_RECORD,
                repository: 'orders',
                id: 100,
                record: {
                    name: 'Jim'
                }
            });

            expect(newState.loggedInUser).to.equal(oldState.loggedInUser);
        });

        it('does not update the logged user when it is not already in state', () => {
            oldState.loggedInUser = undefined;

            newState = reduce(oldState, {
                type: types.REPOSITORY_UPDATE_RECORD,
                repository: 'users',
                id: 100,
                record: {
                    name: 'Jim'
                }
            });

            expect(newState.loggedInUser).to.equal(undefined);
        });
    });

    it('calls the repositories reducer for all other types', () => {
        const record = {
            name: 'Jim'
        };

        const state = reduce({}, {
            type: types.REPOSITORY_GET_RECORD,
            repository: 'users',
            record
        });

        expect(state.record).to.deep.equal(record);
    });
});