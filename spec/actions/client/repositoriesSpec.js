import axios from "axios";
import { getRecords, getRecord, getRecordBy, createRecord, updateRecord, deleteRecord } from "../../../actions/client/repositories";
import * as common from "../../../actions/common";
import * as types from "../../../actions/types";
import sinon from "sinon";
import {expect} from "chai";

describe('repository client actions', () => {
    var dispatch;

    beforeEach(() => {
        dispatch = sinon.stub();
        sinon.stub(common, 'errorHandler').returns('errorHandlerReturn');
        sinon.stub(common, 'notFound').returns('notFoundReturn');
        sinon.stub(axios, 'get');
    });

    afterEach(() => {
        common.errorHandler.restore();
        common.notFound.restore();
        axios.get.restore();
    });

    describe('getRecords', () => {
        let headers;

        beforeEach(() => {
            headers = {
                "record-pagination": "{}",
            };

            axios.get.returns(Promise.resolve({
                data: ['records'],
                headers
            }));
        });

        it('calls axios.get', () => {
            getRecords('repositoryName')(dispatch);

            sinon.assert.calledWith(axios.get, `/repositoryName`);
        });

        it('calls axios.get with the params', () => {
            getRecords('repositoryName', { name: 'bob' })(dispatch);

            sinon.assert.calledWith(axios.get, `/repositoryName`, {
                params: {
                    name: 'bob'
                },
                headers: {
                    "record-order": '{}',
                    "record-page-no": '',
                    "record-per-page": ''
                }
            });
        });

        it('calls axios.get with the order header', () => {
            getRecords('repositoryName', undefined, {
                salary: true
            })(dispatch);

            sinon.assert.calledWith(axios.get, `/repositoryName`, {
                params: {},
                headers: {
                    "record-order": JSON.stringify({
                        salary: true
                    }),
                    "record-page-no": '',
                    "record-per-page": ''
                }
            });
        });

        it('calls axios.get with the pagination values', () => {
            getRecords('repositoryName', undefined, undefined, 3, 4)(dispatch);

            sinon.assert.calledWith(axios.get, `/repositoryName`, {
                params: {},
                headers: {
                    "record-order": '{}',
                    "record-page-no": 3,
                    "record-per-page": 4
                }
            });
        });

        it('dispatches the records', (done) => {
            getRecords('repositoryName')(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORDS,
                    repository: 'repositoryName',
                    records: ['records'],
                    orderColumns: {},
                    pagination: {}
                });

                done();
            }).catch(done);
        });

        it('dispatches the order columns', (done) => {
            getRecords('repositoryName', undefined, {
                salary: true
            })(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORDS,
                    repository: 'repositoryName',
                    records: ['records'],
                    orderColumns: {
                        salary: true
                    },
                    pagination: {}
                });

                done();
            }).catch(done);
        });

        it('dispatches the pagination values', (done) => {
            headers['record-pagination'] = JSON.stringify({
                totalRecords: 3
            });

            getRecords('repositoryName')(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORDS,
                    repository: 'repositoryName',
                    records: ['records'],
                    orderColumns: {},
                    pagination: {
                        totalRecords: 3
                    }
                });

                done();
            }).catch(done);
        });

        it('calls notFound with the wrong repository', (done) => {
            axios.get.returns(Promise.reject({
                response: {
                    status: 404
                }
            }));

            getRecords('doesntExist')(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');

            error.response = {
                status: 500
            };

            dispatch.throws(error);
            axios.get.returns(Promise.reject(error));

            getRecords('repositoryName')(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });

        it('calls the error handler if a non-HTTP error occurs', (done) => {
            const error = new Error('Error message');

            dispatch.throws(error);
            axios.get.returns(Promise.reject(error));

            getRecords('repositoryName')(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('getRecord', () => {
        beforeEach(() => {
            axios.get.returns(Promise.resolve({
                data: 'record'
            }));
        });

        it('dispatches the record', (done) => {
            getRecord('repositoryName', 5)(dispatch).then(() => {
                sinon.assert.calledWith(axios.get, `/repositoryName/5`);

                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORD,
                    repository: 'repositoryName',
                    record: 'record'
                });

                done();
            });
        });

        it('calls notFound with the wrong repository or record', (done) => {
            axios.get.returns(Promise.reject({
                response: {
                    status: 404
                }
            }));

            getRecord('repositoryName', 7)(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');

            error.response = {
                status: 500
            };

            dispatch.throws(error);
            axios.get.returns(Promise.reject(error));

            getRecord('repositoryName', 8)(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('getRecordBy', () => {
        beforeEach(() => {
            axios.get.returns(Promise.resolve({
                data: ['record1', 'record2']
            }));
        });

        it('calls axios.get with the params', () => {
            getRecordBy('repositoryName', { name: 'bob' })(dispatch);

            sinon.assert.calledWith(axios.get, `/repositoryName`, {
                params: {
                    name: 'bob'
                },
                headers: {
                    "record-order": "{}"
                }
            });
        });

        it('calls axios.get with the order header', () => {
            getRecordBy('repositoryName', undefined, {
                salary: true
            })(dispatch);

            sinon.assert.calledWith(axios.get, `/repositoryName`, {
                params: {},
                headers: {
                    "record-order": JSON.stringify({
                        salary: true
                    })
                }
            });
        });

        it('dispatches the records', (done) => {
            getRecordBy('repositoryName', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORD,
                    repository: 'repositoryName',
                    record: 'record1'
                });

                done();
            }).catch(done);
        });

        it('calls notFound with the wrong repository', (done) => {
            axios.get.returns(Promise.reject({
                response: {
                    status: 404
                }
            }));

            getRecordBy('doesntExist', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls notFound with no record returned', (done) => {
            axios.get.returns(Promise.resolve({
                data: []
            }));

            getRecordBy('doesntExist', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            }).catch(done);
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');

            error.response = {
                status: 500
            };

            dispatch.throws(error);
            axios.get.returns(Promise.reject(error));

            getRecordBy('doesntExist', { name: 'bob' })(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('createRecord', () => {
        var record;

        beforeEach(() => {
            sinon.stub(axios, 'post').returns(Promise.resolve({
                headers: {
                    location: '/repositoryName/201'
                }
            }));

            record = {
                name: 'Jim'
            };
        });

        afterEach(() => {
            axios.post.restore();
        });

        it('dispatches the created record', (done) => {
            createRecord('repositoryName', record)(dispatch).then(() => {
                sinon.assert.calledWith(axios.post, `/repositoryName`, record);

                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_CREATE_RECORD,
                    repository: 'repositoryName',
                    record,
                    id: 201
                });

                done();
            });
        });

        it('calls notFound with the wrong repository', (done) => {
            axios.post.returns(Promise.reject({
                response: {
                    status: 404
                }
            }));

            createRecord('doesntExist', { name: 'Jim' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');

            error.response = {
                status: 500
            };

            dispatch.throws(error);
            axios.post.returns(Promise.reject(error));

            createRecord('repositoryName', { name: 'Jim' })(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('updateRecord', () => {
        var record;

        beforeEach(() => {
            record = {
                name: 'Jim'
            };

            sinon.stub(axios, 'put').returns(Promise.resolve());
        });

        afterEach(() => {
            axios.put.restore();
        });

        it('dispatches the updated record', (done) => {
            updateRecord('repositoryName', 56, record)(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_UPDATE_RECORD,
                    repository: 'repositoryName',
                    record,
                    id: 56
                });

                done();
            }).catch(done);
        });

        it('calls axios.put', (done) => {
            updateRecord('repositoryName', 57, record)(dispatch).then(() => {
                sinon.assert.calledWith(axios.put, `/repositoryName/57`, record);

                done();
            });
        });

        it('calls notFound with an non-existent record', (done) => {
            axios.put.returns(Promise.reject({
                response: {
                    status: 404
                }
            }));

            updateRecord('repositoryName', 404, { name: 'Jim' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');

            error.response = {
                status: 500
            };

            dispatch.throws(error);
            axios.put.returns(Promise.reject(error));

            updateRecord('repositoryName', 500, { name: 'Jim' })(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('deleteRecord', () => {
        var record;

        beforeEach(() => {
            record = {
                name: 'Jim'
            };

            sinon.stub(axios, 'delete').returns(Promise.resolve());
        });

        afterEach(() => {
            axios.delete.restore();
        });

        it('dispatches the deleted record', (done) => {
            deleteRecord('repositoryName', 56, record)(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_DELETE_RECORD,
                    repository: 'repositoryName',
                    record,
                    id: 56
                });

                done();
            }).catch(done);
        });

        it('calls axios.delete', (done) => {
            deleteRecord('repositoryName', 56, record)(dispatch).then(() => {
                sinon.assert.calledWith(axios.delete, `/repositoryName/56`);

                done();
            }).catch(done);
        });

        it('calls notFound with an non-existent record', (done) => {
            axios.delete.returns(Promise.reject({
                response: {
                    status: 404
                }
            }));

            deleteRecord('doesntExist', 56, record)(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            }).catch(done);
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');

            error.response = {
                status: 500
            };

            dispatch.throws(error);
            axios.delete.returns(Promise.reject(error));

            deleteRecord('repositoryName', 56, record)(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            }).catch(done);
        });
    });
});