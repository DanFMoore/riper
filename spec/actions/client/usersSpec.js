import { register, login, logout, saveBillingDetails } from "../../../actions/client/users";
import * as repositories from "../../../actions/client/repositories";
import * as types from "../../../actions/types";
import axios from "axios";
import sinon from "sinon";
import { expect } from "chai";
import { createTestStore } from "../../../redux";

describe('user client actions', () => {
    var dispatch, func, user, data;

    beforeEach(() => {
        user = {
            username: 'dan123',
            password: 'testing123'
        };

        data = {
            loginFailureMessage: 'You done gone logged in wrong',
            actionType: types.USER_AUTH,
            token: 'abc123',
            loggedInUser: {
                username: 'bob'
            }
        };

        const promise = Promise.resolve({data});

        sinon.stub(axios, 'post').returns(promise);
        dispatch = sinon.stub().returns(Promise.resolve());
    });

    afterEach(() => {
        axios.post.restore();
    });

    describe('login', () => {
        beforeEach(() => {
            axios.defaults.headers.Authorization = 'a';
            func = login(user);
        });

        it('posts to /users/login', () => {
            func(dispatch);

            sinon.assert.calledWith(axios.post, '/users/login', user);
        });

        it('sets the token as the auth header', done => {
            func(dispatch).then(() => {
                expect(axios.defaults.headers['Authorization']).to.equal('abc123');

                done();
            });
        });

        it('removes the auth header if there no token', done => {
            data.token = null;

            func(dispatch).then(() => {
                expect(Object.keys(axios.defaults.headers)).not.to.contain('Authorization');

                done();
            }).catch(done);
        });

        it('dispatches the data', done => {
            func(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    loginFailureMessage: 'You done gone logged in wrong',
                    type: types.USER_AUTH,
                    token: 'abc123',
                    loggedInUser: {
                        username: 'bob'
                    }
                });

                done();
            });
        });
    });

    describe('register', () => {
        beforeEach(() => {
            func = register(user);

            sinon.stub(repositories, 'createRecord').returns('createRecordReturn');
        });

        afterEach(() => {
            repositories.createRecord.restore();
        });

        it('dispatches createRecord', done => {
            func(dispatch).then(() => {
                sinon.assert.calledWith(repositories.createRecord, 'users', user);
                sinon.assert.calledWith(dispatch, 'createRecordReturn');

                done();
            });
        });

        it('calls login', done => {
            dispatch.callsFake(func => {
                if (typeof func === 'function') {
                    func(dispatch);
                }

                return Promise.resolve();
            });

            func(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    loginFailureMessage: 'You done gone logged in wrong',
                    type: types.USER_AUTH,
                    token: 'abc123',
                    loggedInUser: {
                        username: 'bob'
                    }
                });

                done();
            });
        });

        it('dispatches USER_REGISTER', done => {
            func(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.USER_REGISTER
                });

                done();
            });
        });
    });

    describe('logout', () => {
        var store;

        it('logs out if logged in', () => {
            store = createTestStore({
                users: {
                    loggedInUser: {
                        id: 100
                    }
                }
            });

            store.dispatch(logout());

            expect(store.getState().actions[0]).to.deep.equal({
                type: types.USER_LOGOUT
            });
        });

        it('does not log out if not logged in', () => {
            store = createTestStore({
                users: {}
            });

            store.dispatch(logout());

            expect(store.getState().actions.length).to.equal(0);
        });
    });

    describe('saveBillingDetails', () => {
        var store, user;

        beforeEach(() => {
            store = createTestStore({
                users: {
                    loggedInUser: {
                        id: 100
                    }
                }
            });

            user = {
                address: 'Road Lane'
            };

            sinon.stub(axios, 'put').returns(Promise.resolve());
        });

        afterEach(() => {
            axios.put.restore();
        });

        it('calls axios.put', () => {
            store.dispatch(saveBillingDetails(user));

            sinon.assert.calledWith(axios.put, '/users/billing-details', user);
        });

        it('dispatches REPOSITORY_UPDATE_RECORD', done => {
            store.dispatch(saveBillingDetails(user)).then(() => {
                expect(store.getState().actions[0]).to.deep.equal({
                    type: types.REPOSITORY_UPDATE_RECORD,
                    repository: 'users',
                    record: user,
                    id: 100
                });

                done();
            }).catch(done);
        })
    });
});