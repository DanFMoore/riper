import * as users from "../../actions/users";
import * as common from "../../actions/common";
import {expect} from "chai";
import sinon from "sinon";

describe('user actions', () => {
    var state, dispatch, getState, error;

    beforeEach(() => {
        state = {
            users: {}
        };

        dispatch = sinon.stub();
        getState = sinon.stub().returns(state);
    });

    describe('forbidden', () => {
        beforeEach(() => {
            sinon.stub(common, 'errorHandler').callsFake(e => {
                error = e;

                return 'errorHandlerReturn';
            });
        });

        afterEach(() => {
            common.errorHandler.restore();
        });

        it('dispatches the error handler', () => {
            users.forbidden()(dispatch, getState);
            sinon.assert.calledWith(dispatch, 'errorHandlerReturn');
        });

        it('passes the 401 error to errorHandler', () => {
            users.forbidden()(dispatch, getState);

            expect(error.message).to.equal('Not logged in');
            expect(error.status).to.equal(401);
        });

        it('passes the 403 error to errorHandler', () => {
            state.users.loggedInUser = 'user';
            users.forbidden()(dispatch, getState);

            expect(error.message).to.equal('Access forbidden');
            expect(error.status).to.equal(403);
        });
    });

    describe('checkUserPermission', () => {
        beforeEach(() => {
            state.users.loggedInUser = {
                id: 200
            };
        });

        it('does not dispatch anything if the requested user id is the same as the logged in user', () => {
            users.checkUserPermission('200')(dispatch, getState);
            sinon.assert.notCalled(dispatch);
        });

        it('dispatches forbidden if the request user id is not the same', done => {
            // Bit of a hack to make sure the right function was dispatched.
            // Would use sinon.stub(users, 'forbidden'), but it's in the same file so that won't work.
            function dispatch(func) {
                expect(func.name).to.equal('forbiddenDispatch');

                done();
            }

            users.checkUserPermission(201)(dispatch, getState);
        });

        it('dispatches forbidden if there is no logged in user', done => {
            state.users.loggedInUser = undefined;

            function dispatch(func) {
                expect(func.name).to.equal('forbiddenDispatch');

                done();
            }

            users.checkUserPermission(201)(dispatch, getState);
        })
    });
});