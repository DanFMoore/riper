import * as types from "../../../actions/types";
import sinon from "sinon";
import { expect } from "chai";
import { validate, filter } from "../../../actions/server/request";
import { createTestStore } from "../../../redux";

describe('request server actions', () => {
    var dispatch, state, getState;

    beforeEach(() => {
        state = {
            request: {
                body: {
                    name: 'Jim',
                    email: 'eaaweaw@awe.com'
                }
            }
        };

        dispatch = sinon.stub();
        getState = sinon.stub().returns(state);
    });

    describe('filter', () => {
        var callback, store;

        beforeEach(() => {
            store = createTestStore(state);
        });

        describe('with a promise callback', () => {
            beforeEach(() => {
                callback = sinon.stub().callsFake(() => dispatch => {
                    dispatch({
                        type: types.REQUEST_STORE_BODY,
                        body: {
                            name: 'Bob',
                            email: 'test@test.com',
                        }
                    });

                    return Promise.resolve();
                });
            });

            assert();
        });

        describe('without a promise', () => {
            beforeEach(() => {
                callback = sinon.stub().returns({
                    type: types.REQUEST_STORE_BODY,
                    body: {
                        name: 'Bob',
                        email: 'test@test.com',
                    }
                });
            });

            assert();
        });

        function assert() {
            it('calls the callback with the body', () => {
                store.dispatch(filter(callback));

                sinon.assert.calledWith(callback, state.request.body);
            });

            it('dispatches the filtered body from the callback', done => {
                store.dispatch(filter(callback)).then(() => {
                    expect(store.getState().actions[0]).to.deep.equal({
                        type: types.REQUEST_STORE_BODY,
                        body: {
                            name: 'Bob',
                            email: 'test@test.com',
                        }
                    });

                    done();
                }).catch(done);
            });

            it('does not dispatch the filtered body if the request is invalid', done => {
                state.request.invalid = true;

                store.dispatch(filter(callback)).then(() => {
                    expect(store.getState().actions.length).to.equal(0);

                    done();
                }).catch(done);
            });
        }
    });

    describe('validate', () => {
        var syncValidation, asyncValidation;

        beforeEach(() => {
            syncValidation = sinon.stub().returns({});
            asyncValidation = sinon.stub().returns(Promise.resolve());
        });

        describe('syncValidation', () => {
            it('calls the function', () => {
                validate(syncValidation, asyncValidation)(dispatch, getState);

                sinon.assert.calledWith(syncValidation, state.request.body);
            });

            it('dispatches no errors', () => {
                validate(syncValidation, asyncValidation)(dispatch, getState);

                sinon.assert.calledWith(dispatch, {
                    type: types.REQUEST_VALIDATE,
                    errors: {}
                });
            });

            it('dispatches errors', () => {
                syncValidation.returns({
                    name: 'Required'
                });

                validate(syncValidation, asyncValidation)(dispatch, getState);

                sinon.assert.calledWith(dispatch, {
                    type: types.REQUEST_VALIDATE,
                    errors: {
                        name: 'Required'
                    },
                });
            });
        });

        describe('asyncValidation', () => {
            it('calls the function', done => {
                validate(syncValidation, asyncValidation)(dispatch, getState).then(() => {
                    sinon.assert.calledWith(asyncValidation, state.request.body);

                    done();
                });
            });

            it('does not throw an error if asyncValidation is not passed', () => {
                validate(syncValidation)(dispatch, getState);
            });

            it('does not dispatch if there are no errors', done => {
                validate(syncValidation, asyncValidation)(dispatch, getState).then(() => {
                    sinon.assert.notCalled(dispatch);

                    done();
                });

                // Don't count the sync dispatch call
                dispatch.reset();
            });

            it('dispatches errors', done => {
                asyncValidation.returns(Promise.reject({
                    email: 'This email address is in use'
                }));

                validate(syncValidation, asyncValidation)(dispatch, getState).then(() => {
                    sinon.assert.calledWith(dispatch, {
                        type: types.REQUEST_VALIDATE,
                        errors: {
                            email: 'This email address is in use'
                        }
                    });

                    done();
                });
            });
        });

        describe('body parameter', () => {
            it('calls REQUEST_STORE_BODY', () => {
                validate(syncValidation, asyncValidation, {
                    name: 'Clive',
                    email: 'clive@awe.com'
                })(dispatch, getState);

                sinon.assert.calledWith(dispatch, {
                    type: types.REQUEST_STORE_BODY,
                    body: {
                        name: 'Clive',
                        email: 'clive@awe.com'
                    }
                });
            });
        });
    });
});