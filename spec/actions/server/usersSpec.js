import getActions from "../../../actions/server/users";
import * as types from "../../../actions/types";
import sinon from "sinon";
import {expect} from "chai";
import Password from '../../../auth/password';
import jwt from 'jsonwebtoken';
import settings from '../../../resources/settings';

describe('users server actions', () => {
    var dispatch, func, actions, users;

    beforeEach(() => {
        dispatch = sinon.stub().returns(Promise.resolve());

        users = {
            getOne: sinon.stub().returns(Promise.resolve())
        };

        actions = getActions(users);
    });

    describe('login', () => {
        beforeEach(() => {
            func = actions.login('bob', 'password123');
        });

        it('calls users.getOne', () => {
            func(dispatch);

            sinon.assert.calledWith(users.getOne, {
                username: 'bob',
                active: true
            });
        });

        it('dispatches USERNAME_NOT_FOUND when there is no user returned', done => {
            func(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.USERNAME_NOT_FOUND,
                });

                done();
            })
        });

        it('dispatches the user with the right password', done => {
            var user;

            Password('password123').hash().then(hash => {
                user = {
                    id: 10,
                    username: 'bob',
                    password: hash,
                    active: true
                };

                users.getOne.returns(Promise.resolve(user));

                return func(dispatch);
            }).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.USER_AUTH,
                    loggedInUser: user,
                    token: jwt.sign(
                        {
                            id: user.id,
                            password: user.password
                        },
                        settings.jwtSecret
                    )
                });

                done();
            });
        });

        it('does not dispatch the user with the wrong password', done => {
            const user = {
                id: 10,
                username: 'bob',
                password: 'non-matching-hash',
                active: true
            };

            users.getOne.returns(Promise.resolve(user));

            func(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.USER_AUTH,
                    loggedInUser: null,
                    token: null
                });

                done();
            });
        });
    });

    describe('getUserByToken', () => {
        beforeEach(() => {
            const user = {
                id: 200,
                password: 'secret-hash'
            };

            const token = jwt.sign(
                {
                    id: user.id,
                    password: user.password
                },
                settings.jwtSecret
            );

            func = actions.getUserByToken(token);
        });

        it('calls user.getOne', () => {
            func(dispatch);

            sinon.assert.calledWith(users.getOne, {
                id: 200,
                password: 'secret-hash',
                active: true
            });
        });

        it('dispatches USER_TOKEN_EXPIRED if user not returned', done => {
            func(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.USER_TOKEN_EXPIRED,
                });

                done();
            });
        });

        it('dispatches the user if it is returned', done => {
            const user = {
                id: 200,
                username: 'bob',
                active: true
            };

            users.getOne.returns(Promise.resolve(user));

            func(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.USER_AUTH,
                    loggedInUser: user,
                    result: true
                });

                done();
            }).catch(done);
        });
    });
});