import getActions from "../../../actions/server/repositories";
import * as common from "../../../actions/common";
import * as types from "../../../actions/types";
import sinon from "sinon";
import {expect} from "chai";

describe('server actions', () => {
    var dispatch, actions, repositories;

    beforeEach(() => {
        dispatch = sinon.stub();
        sinon.stub(common, 'errorHandler').returns('errorHandlerReturn');
        sinon.stub(common, 'notFound').returns('notFoundReturn');

        repositories = {};
        actions = getActions(repositories);
    });

    afterEach(() => {
        common.errorHandler.restore();
        common.notFound.restore();
    });

    describe('getRecords', () => {
        let records;

        beforeEach(() => {
            records = ['record'];

            repositories.repositoryName = {
                getAll: sinon.stub().returns(Promise.resolve(records))
            };
        });

        it('dispatches the records', (done) => {
            actions.getRecords('repositoryName')(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORDS,
                    repository: 'repositoryName',
                    records: ['record'],
                    orderColumns: {},
                    pagination: undefined
                });

                done();
            }).catch(done);
        });

        it('calls getAll with the params', done => {
            actions.getRecords('repositoryName', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(repositories.repositoryName.getAll, { name: 'bob' });

                done();
            }).catch(done);
        });

        it('dispatches the order columns', done => {
            actions.getRecords('repositoryName', undefined, { salary: true })(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORDS,
                    repository: 'repositoryName',
                    records: ['record'],
                    orderColumns: { salary: true },
                    pagination: undefined
                });

                done();
            }).catch(done);
        });

        it('calls getAll with the order columns', done => {
            actions.getRecords('repositoryName', undefined, { salary: true })(dispatch).then(() => {
                sinon.assert.calledWith(repositories.repositoryName.getAll, undefined, { salary: true });

                done();
            }).catch(done);
        });

        it('dispatches the pagination values', done => {
            records.pagination = {
                perPage: 2
            };

            actions.getRecords('repositoryName')(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORDS,
                    repository: 'repositoryName',
                    records: ['record'],
                    orderColumns: {},
                    pagination: {
                        perPage: 2
                    }
                });

                done();
            }).catch(done);
        });

        it('calls getAll with the pagination values', done => {
            actions.getRecords('repositoryName', undefined, undefined, 3, 4)(dispatch).then(() => {
                sinon.assert.calledWith(repositories.repositoryName.getAll, undefined, {}, 3, 4);

                done();
            }).catch(done);
        });

        it('calls notFound with the wrong repository', (done) => {
            actions.getRecords('doesntExist')(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            }).catch(done);
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');
            dispatch.throws(error);

            repositories.repositoryName = {
                getAll: sinon.stub().returns(Promise.reject(error))
            };

            actions.getRecords('repositoryName')(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('getRecordBy', () => {
        beforeEach(() => {
            repositories.repositoryName = {
                getOne: sinon.stub().returns(Promise.resolve('record1'))
            };
        });

        it('dispatches the records', done => {
            actions.getRecordBy('repositoryName', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORD,
                    repository: 'repositoryName',
                    record: 'record1'
                });

                done();
            }).catch(done);
        });

        it('calls getOne with the params', done => {
            actions.getRecordBy('repositoryName', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(repositories.repositoryName.getOne, { name: 'bob' });

                done();
            }).catch(done);
        });

        it('calls getAll with the order columns', done => {
            actions.getRecordBy('repositoryName', undefined, { salary: true })(dispatch).then(() => {
                sinon.assert.calledWith(repositories.repositoryName.getOne, undefined, { salary: true });

                done();
            }).catch(done);
        });

        it('calls notFound with an non-existent record', (done) => {
            repositories.repositoryName.getOne.returns(Promise.resolve(null));

            actions.getRecordBy('repositoryName', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            }).catch(done);
        });

        it('calls notFound with the wrong repository', (done) => {
            actions.getRecordBy('doesntExist', { name: 'bob' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');
            dispatch.throws(error);

            repositories.repositoryName = {
                getOne: sinon.stub().returns(Promise.reject(error))
            };

            actions.getRecordBy('repositoryName', { name: 'bob' })(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('getRecord', () => {
        beforeEach(() => {
            repositories.repositoryName = {
                getById: (id) => Promise.resolve('record' + id)
            };
        });

        it('dispatches the record', (done) => {
            actions.getRecord('repositoryName', 5)(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_GET_RECORD,
                    repository: 'repositoryName',
                    record: 'record5'
                });

                done();
            }).catch(done);
        });

        it('calls notFound with the wrong repository', (done) => {
            actions.getRecord('doesntExist', 7)(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls notFound with an non-existent record', (done) => {
            repositories.repositoryName = {
                getById: (id) => Promise.resolve(null)
            };

            actions.getRecord('repositoryName', 404)(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');
            dispatch.throws(error);

            repositories.repositoryName = {
                getById: () => Promise.reject(error)
            };

            actions.getRecord('repositoryName', 8)(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('createRecord', () => {
        var record;

        beforeEach(() => {
            record = {
                name: 'Jim'
            };

            repositories.repositoryName = {
                insert: () => Promise.resolve(201)
            };
        });

        it('dispatches the created record', (done) => {
            actions.createRecord('repositoryName', record)(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_CREATE_RECORD,
                    repository: 'repositoryName',
                    record,
                    id: 201
                });

                done();
            });
        });

        it('calls notFound with the wrong repository', (done) => {
            actions.createRecord('doesntExist', { name: 'Jim' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');
            dispatch.throws(error);

            repositories.repositoryName = {
                insert: sinon.stub().returns(Promise.reject(error))
            };

            actions.createRecord('repositoryName', { name: 'Jim' })(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('updateRecord', () => {
        var record;

        beforeEach(() => {
            record = {
                name: 'Jim'
            };

            repositories.repositoryName = {
                update: sinon.stub().returns(Promise.resolve(true))
            };
        });

        it('dispatches the updated record', (done) => {
            actions.updateRecord('repositoryName', 56, record)(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_UPDATE_RECORD,
                    repository: 'repositoryName',
                    record,
                    id: 56
                });

                done();
            }).catch(done);
        });

        it('calls repository.update', (done) => {
            actions.updateRecord('repositoryName', 57, record)(dispatch).then(() => {
                sinon.assert.calledWith(repositories.repositoryName.update, 57, record);

                done();
            });
        });

        it('calls notFound with the wrong repository', (done) => {
            actions.updateRecord('doesntExist', 58, { name: 'Jim' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls notFound with an non-existent record', (done) => {
            repositories.repositoryName.update.returns(Promise.resolve(false));

            actions.updateRecord('repositoryName', 404, { name: 'Jim' })(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            });
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');
            dispatch.throws(error);

            repositories.repositoryName.update.returns(Promise.reject(error));

            actions.updateRecord('repositoryName', 500, { name: 'Jim' })(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            });
        });
    });

    describe('deleteRecord', () => {
        var record;

        beforeEach(() => {
            record = {
                name: 'Jim'
            };

            repositories.repositoryName = {
                delete: sinon.stub().returns(Promise.resolve(true))
            };
        });

        it('dispatches the deleted record', (done) => {
            actions.deleteRecord('repositoryName', 50, record)(dispatch).then(() => {
                sinon.assert.calledWith(dispatch, {
                    type: types.REPOSITORY_DELETE_RECORD,
                    repository: 'repositoryName',
                    id: 50,
                    record
                });

                done();
            }).catch(done);
        });

        it('calls repository.delete', (done) => {
            actions.deleteRecord('repositoryName', 50, record)(dispatch).then(() => {
                sinon.assert.calledWith(repositories.repositoryName.delete, 50);

                done();
            }).catch(done);
        });

        it('calls notFound with the wrong repository', (done) => {
            actions.deleteRecord('doesntExist', 50, record)(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            }).catch(done);
        });

        it('calls notFound with an non-existent record', (done) => {
            repositories.repositoryName.delete.returns(Promise.resolve(false));

            actions.deleteRecord('repositoryName', 50, record)(dispatch).then(() => {
                sinon.assert.calledWith(common.notFound);
                sinon.assert.calledWith(dispatch, 'notFoundReturn');

                done();
            }).catch(done);
        });

        it('calls the error handler if an error occurs', (done) => {
            const error = new Error('Error message');
            dispatch.throws(error);

            repositories.repositoryName.delete.returns(Promise.reject(error));

            actions.deleteRecord('repositoryName', 50, record)(dispatch).catch(() => {
                sinon.assert.calledWith(common.errorHandler, error);
                sinon.assert.calledWith(dispatch, 'errorHandlerReturn');

                done();
            }).catch(done);
        });
    });
});