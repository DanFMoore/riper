import { executePaginationQuery } from "../../../actions/server/helpers";
import connection from "../../../resources/connection";
import settings from "../../../resources/settings";
import {expect} from "chai";
import {initDb} from "../../../db";

describe('server action helpers', () => {
    describe('executePaginationQuery', () => {
        const fields = 'id, name';
        const query = 'FROM page_test WHERE id < $1 ORDER BY id';

        before(function(done) {
            settings.recordsPerPage = 2;

            initDb('./spec/fixtures/repository.sql').then(() => {
                done();
            }).catch(done);
        });

        it('limits the records to the first page', done => {
            executePaginationQuery(connection, fields, query, [6], 1).then(result => {
                expect(result.records.length).to.equal(2);
                expect(result.records[0].id).to.equal(1);
                expect(result.records[1].id).to.equal(2);

                done();
            }).catch(done);
        });

        it('limits the records to the second page', done => {
            executePaginationQuery(connection, fields, query, [6], 2).then(result => {
                expect(result.records.length).to.equal(2);
                expect(result.records[0].id).to.equal(3);
                expect(result.records[1].id).to.equal(4);

                done();
            }).catch(done);
        });

        it('limits the records to the last page', done => {
            executePaginationQuery(connection, fields, query, [6], 3).then(result => {
                expect(result.records.length).to.equal(1);
                expect(result.records[0].id).to.equal(5);

                done();
            }).catch(done);
        });

        it('limits the records to a non-default perPage number', done => {
            executePaginationQuery(connection, fields, query, [8], 2, 3).then(result => {
                expect(result.records.length).to.equal(3);
                expect(result.records[0].id).to.equal(4);
                expect(result.records[1].id).to.equal(5);
                expect(result.records[2].id).to.equal(6);

                done();
            }).catch(done);
        });

        it('returns the pagination values', done => {
            executePaginationQuery(connection, fields, query, [6], 3).then(result => {
                expect(result.pagination).to.deep.equal({
                    totalRecords: 5,
                    pageNo: 3,
                    perPage: 2,
                    totalPages: 3
                });

                done();
            }).catch(done);
        });
    });
});