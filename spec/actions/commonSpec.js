import {errorHandler} from "../../actions/common";
import * as types from "../../actions/types";
import {expect} from "chai";

describe('common actions', () => {
    describe('errorHandler', () => {
        it('dispatches the error', (done) => {
            const error = new Error('an error');

            errorHandler(error)(action => {
                expect(action.type).to.equal(types.ERROR);
                expect(action.error).to.equal(error);

                done();
            });
        });

        it('throws the error back', (done) => {
            const error = new Error('an error');

            try {
                errorHandler(error)(() => {});
            } catch (e) {
                expect(e).to.equal(error);

                done();
            }
        });
    });
});