import { expect } from "chai";
import { register } from "../../../actions/filters/user";
import Password from "../../../auth/password";
import { createTestStore } from "../../../redux";

describe('user filters', () => {
    var store;

    beforeEach(() => {
        store = createTestStore();
    });

    describe('register', () => {
        var user, action;

        beforeEach(done => {
            user = {
                username: 'jim',
                email: 'test@test.com',
                name: 'Jim',
                password: 'testing123',
                unexpected: 'item in bagging area'
            };

            store.dispatch(register(user)).then(() => {
                action = store.getState().actions[0];

                done();
            }).catch(done);
        });

        it('dispatches the correct type', () => {
            expect(action.type).to.equal('REQUEST_STORE_BODY');
        });

        it('returns a new instance', () => {
            expect(action.body).not.to.equal(user);
        });

        it('hashes the password', done => {
            Password(user.password).authenticate(action.body.password).then(result => {
                expect(result).to.equal(true);

                done();
            });
        });

        it('sets the join date and sets it to now', () => {
            expect(action.body.joinDate.getTime()).closeTo(Date.now(), 10);
        });

        it('leaves the other fields', () => {
            expect(action.body.username).to.equal('jim');
            expect(action.body.email).to.equal('test@test.com');
            expect(action.body.name).to.equal('Jim');
        });

        it('removes any unexpected fields', () => {
            expect(action.body.unexpected).to.equal(undefined);
        });
    });
});