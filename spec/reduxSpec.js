import { expect } from "chai";
import sinon from "sinon";
import { renderClient, renderServer, reduxStore } from '../redux';
import * as redux from 'redux';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { shallow } from "enzyme";
import ReactDOMServer from 'react-dom/server';

describe('redux', () => {
    var store, subscribeCallback, renderedRouter, returnedFunction, state;

    beforeEach(() => {
        state = {
            users: {
                save: true
            },
            thing: {
                save: false
            }
        };

        store = {
            subscribe: sinon.stub().callsFake((callback) => {
                subscribeCallback = callback;
            }),
            getState: sinon.stub().returns(state),
            dispatch: sinon.stub()
        };

        sinon.stub(redux, 'createStore').returns(store);
        sinon.stub(redux, 'applyMiddleware').returns('middleware');
    });

    afterEach(() => {
        redux.createStore.restore();
        redux.applyMiddleware.restore();
    });

    describe('renderClient', () => {
        var router, pushState, eventName, eventCallback;

        beforeEach(() => {
            sinon.stub(ReactDOM, 'render').callsFake((passedRouter) => {
                router = passedRouter;
            });

            sinon.stub(axios, 'put');
            sinon.stub(window.history, 'pushState');

            sinon.stub(window, 'addEventListener').callsFake((event, callback) => {
                eventName = event;
                eventCallback = callback;
            });

            // Have to store another reference as renderClient overwrites it
            pushState = window.history.pushState;

            window.__REDUX_STATE__ = {
                users: {
                    token: 'my-first-token'
                }
            };

            document.getElementById = sinon.stub().returns('#react-view');

            renderClient('App', 'reduce', 'https://api/');
        });

        afterEach(() => {
            ReactDOM.render.restore();
            axios.put.restore();
            pushState.restore();
            window.addEventListener.restore();
        });

        it('sets the axios defaults', () => {
            expect(axios.defaults.baseURL).to.equal('https://api/');
            expect(axios.defaults.headers['Content-Type']).to.equal('application/json');
            expect(axios.defaults.headers['Accept']).to.equal('application/json');
            expect(axios.defaults.headers['Authorization']).to.equal('my-first-token');
        });

        it('calls createStore', () => {
            sinon.assert.calledWith(redux.applyMiddleware, thunk);
            sinon.assert.calledWith(redux.createStore, 'reduce', { users: { token: 'my-first-token' } }, 'middleware');
        });

        describe('subscribe function', () => {
            it('calls store.subscribe', () => {
                sinon.assert.called(store.subscribe);
            });

            it('sends the specific sub-states to be saved via PUT the first time it is run', () => {
                subscribeCallback();

                sinon.assert.calledWith(
                    axios.put,
                    '/redux-state',
                    {
                        users: {
                            save: true
                        }
                    },
                    {
                        baseURL: 'https://example.com'
                    }
                );
            });

            it('does not send the specific sub-states later when they have not changed', () => {
                subscribeCallback();

                // Change the non-savable state
                state.thing = {
                    ...state.thing,
                    key: 'hello'
                };

                subscribeCallback();
                expect(axios.put.callCount).to.equal(1);
            });

            it('sends the specific sub-states to be saved if they have changed', () => {
                subscribeCallback();

                // Change the savable state
                state.users = {
                    ...state.users,
                    key: 'hello'
                };

                subscribeCallback();
                expect(axios.put.callCount).to.equal(2);
            });
        });

        it('deletes the window redux state', () => {
            expect(window.__REDUX_STATE__).to.equal(undefined);
        });

        describe('router', () => {
            beforeEach(() => {
                renderedRouter = shallow(router);
            });

            it('is rendered', () => {
                sinon.assert.calledWith(ReactDOM.render, router, '#react-view');
            });

            it('contains a Router component', () => {
                expect(renderedRouter.find('Router').length).to.equal(1);
            });

            it('contains a Provider component', () => {
                const provider = renderedRouter.find('Router Provider');

                expect(provider.length).to.equal(1);
                expect(provider.props().store).to.equal(store);
            });

            it('contains an App component', () => {
                expect(renderedRouter.find('Router Provider App').length).to.equal(1);
            });
        });

        describe('pushState', () => {
            it('calls the original pushState function', () => {
                window.history.pushState('arg1', 'arg2');

                sinon.assert.calledWith(pushState, 'arg1', 'arg2');
            });

            it('dispatches clearError', () => {
                window.history.pushState();

                sinon.assert.calledWith(store.dispatch, {
                    type: 'CLEAR_ERROR'
                });
            })
        });

        describe('popstate', () => {
            it('adds a popstate event', () => {
                expect(eventName).to.equal('popstate');
            });

            it('dispatches clearError', () =>{
                eventCallback();

                sinon.assert.calledWith(store.dispatch, {
                    type: 'CLEAR_ERROR'
                });
            });
        });
    });

    describe('renderServer', () => {
        var req, res, context;

        beforeEach(() => {
            req = {
                originalUrl: '/about'
            };

            res = {
                redirect: sinon.stub(),
                status: sinon.stub(),
                render: sinon.stub(),
                locals: { store }
            };

            context = {
                status: 200
            };

            sinon.stub(ReactDOMServer, 'renderToString').returns('html');
        });

        afterEach(() => {
            ReactDOMServer.renderToString.restore();
        });

        describe('StaticRouter', () => {
            beforeEach(() => {
                returnedFunction = renderServer('App', undefined, context);
                renderedRouter = shallow(returnedFunction(req, res));
            });

            it('is created', () => {
                expect(renderedRouter.find('Router').length).to.equal(1);
            });

            it('is passed the location', () => {
                expect(renderedRouter.props().history.location.pathname).to.equal('/about');
            });

            it('is passed the context', () => {
                expect(renderedRouter.unrendered.props.context.status).to.equal(200);
            });

            it('contains a Provider component', () => {
                const provider = renderedRouter.find('Router Provider');

                expect(provider.length).to.equal(1);
                expect(provider.props().store).to.equal(store);
            });

            it('contains an App component', () => {
                expect(renderedRouter.find('Router Provider App').length).to.equal(1);
            });
        });

        describe('with a context url', () => {
            beforeEach(() => {
                context = {
                    url: '/help'
                };

                returnedFunction = renderServer('App', undefined, context);
                returnedFunction(req, res);
            });

            it('redirects to the context url', () => {
                sinon.assert.calledWith(res.redirect, 301, '/help');
            });
        });

        describe('without a context url', () => {
            beforeEach(() => {
                returnedFunction = renderServer('App', undefined, context);
                renderedRouter = returnedFunction(req, res);
            });

            it('renders the static router', () => {
                sinon.assert.calledWith(ReactDOMServer.renderToString, renderedRouter);
            });

            it('sets the http status', () => {
                sinon.assert.calledWith(res.status, 200);
            });

            it('renders the layout with the html and state', () => {
                sinon.assert.calledWith(res.render, 'layout', {
                    html: 'html',
                    state: JSON.stringify(state),
                    rawState: state
                });
            });

            it('uses the layout parameter', () => {
                returnedFunction = renderServer('App', 'anotherLayout', context);
                renderedRouter = returnedFunction(req, res);

                sinon.assert.calledWith(res.render, 'anotherLayout', {
                    html: 'html',
                    state: JSON.stringify(state),
                    rawState: state
                });
            });
        });
    });

    describe('reduxStore', () => {
        var req, res, next;

        beforeEach(() => {
            returnedFunction = reduxStore('reduce');
            next = sinon.stub();

            res = {
                locals: {},
                send: sinon.stub()
            }
        });

        describe('PUT /redux-state', () => {
            beforeEach(() => {
                req = {
                    method: 'PUT',
                    url: '/redux-state',
                    session: {},
                    body: {
                        key: 'value'
                    }
                };

                returnedFunction(req, res, next);
            });

            it('sets the redux state into the session', () => {
                expect(req.session.reduxState).to.deep.equal(req.body);
            });

            it('calls res.send', () => {
                sinon.assert.called(res.send);
            });

            it('does not call next', () => {
                sinon.assert.notCalled(next);
            });
        });

        describe('else', () => {
            beforeEach(() => {
                req = {
                    session: { reduxState: 'initialState' }
                };

                returnedFunction(req, res, next);
            });

            it('calls createStore', () => {
                sinon.assert.calledWith(redux.applyMiddleware, thunk);
                sinon.assert.calledWith(redux.createStore, 'reduce', req.session.reduxState, 'middleware');
            });

            it('calls next', () => {
                sinon.assert.called(next);
            });

            it('does not call res.send', () => {
                sinon.assert.notCalled(res.send);
            });
        });
    });
});