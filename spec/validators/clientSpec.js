import { expect } from "chai";
import validateOnClient from "../../validators/client";
import axios from "axios";
import sinon from "sinon";

describe('validateOnClient', () => {
    var resource, func;

    beforeEach(() => {
        resource = {
            username: 'jim',
            email: 'test@test.com'
        };

        sinon.stub(axios, 'post').returns(Promise.resolve());
        sinon.stub(axios, 'put').returns(Promise.resolve());

        func = validateOnClient('/resource');
    });

    afterEach(() => {
        axios.post.restore();
        axios.put.restore();
    });

    it('posts the resource to the resource end point', () => {
        func(resource);

        sinon.assert.calledWith(axios.post, '/resource', resource, {
            headers: {
                'Validate-Only': 1
            }
        });

        sinon.assert.notCalled(axios.put);
    });

    it('puts the resource to the resource end point', () => {
        validateOnClient('/put-resource', true)(resource);

        sinon.assert.calledWith(axios.put, '/put-resource', resource, {
            headers: {
                'Validate-Only': 1
            }
        });

        sinon.assert.notCalled(axios.post);
    });

    it('returns an empty object if there are no errors', done => {
        func(resource).then(result => {
            expect(result).to.deep.equal({});

            done();
        });
    });

    it('returns the errors if axios rejects the promise', done => {
        axios.post.returns(Promise.reject({
            response: {
                data: {
                    email: 'Silly email',
                    name: 'You have a silly name'
                }
            }
        }));

        func(resource).then(result => {
            expect(result).to.deep.equal({
                email: 'Silly email',
                name: 'You have a silly name'
            });

            done();
        });
    });
});