import { expect } from "chai";
import getValidators from "../../../validators/server/user";
import sinon from "sinon";

describe('server user validators', () => {
    describe('register', () => {
        var user, users, validators;

        beforeEach(() => {
            user = {
                username: 'jim',
                email: 'test@test.com'
            };

            users = {
                getOne: sinon.stub().returns(Promise.resolve())
            };

            validators = getValidators(users);
        });

        checkRejections(true, false);
        checkRejections(false, true);
        checkRejections(true, true);

        it('calls users.getOne for the username', () => {
            validators.register(user);

            sinon.assert.calledWith(users.getOne, {
                username: user.username,
                active: true
            });
        });

        it('calls users.getOne for the email', () => {
            validators.register(user);

            sinon.assert.calledWith(users.getOne, {
                email: user.email,
                active: true
            });
        });

        it('resolves with nothing if there are no errors', done => {
            validators.register(user).then(result => {
                expect(result).to.equal(undefined);

                done();
            });
        });

        function checkRejections(username, email) {
            describe('errors for ' + (username ? 'username, ' : '') + (email ? 'email' : ''), () => {
                var result;

                beforeEach(done => {
                    users.getOne.callsFake(criteria => {
                        if (criteria.username && username) {
                            return Promise.resolve('user');
                        } else if (criteria.email && email) {
                            return Promise.resolve('email');
                        } else {
                            return Promise.resolve();
                        }
                    });

                    validators.register(user).catch(errors => {
                        result = errors;
                        done();
                    });
                });

                if (username) {
                    it('contains a username error', () => {
                        expect(result.username).to.equal('This username is in use');
                    });

                    if (!email) {
                        it('does not contain an email error', () => {
                            expect(result.email).to.equal(undefined);
                        })
                    }
                }

                if (email) {
                    it('contains an email error', () => {
                        expect(result.email).to.equal('This email address is in use');
                    });

                    if (!username) {
                        it('does not contain a username error', () => {
                            expect(result.username).to.equal(undefined);
                        })
                    }
                }
            });
        }
    });
});