import { expect } from "chai";
import { register, address } from "../../validators/user";

describe('synchronous user validators', () => {
    var errors;

    describe('register', () => {
        it('returns an empty object if the values are all valid', () => {
            errors = register({
                username: 'jim',
                email: 'test@test.com',
                name: 'Jim',
                password: 'testing123',
                confirmPassword: 'testing123'
            });

            expect(errors).to.deep.equal({});
        });

        checkMissingField('username');
        checkMissingField('email');
        checkMissingField('password');
        checkMissingField('confirmPassword');

        function checkMissingField(field) {
            it('returns ' + field + ' if it is not filled in', () => {
                const user = {
                    username: 'jim',
                    email: 'test@test.com',
                    password: 'testing123',
                    confirmPassword: 'testing123'
                };

                user[field] = '';
                errors = register(user);

                expect(Object.keys(errors).length).to.equal(field === 'password' ? 2 : 1);
                expect(errors[field]).to.equal('Required');
            });
        }

        it('validates the email format', () => {
            const user = {
                username: 'jim',
                email: 'testtest.com',
                name: 'Jim',
                password: 'testing123',
                confirmPassword: 'testing123'
            };

            errors = register(user);

            expect(errors).to.deep.equal({
                email: 'Invalid email address'
            });
        });

        it('validates the confirm password field', () => {
            const user = {
                username: 'jim',
                email: 'test@test.com',
                name: 'Jim',
                password: 'testing123',
                confirmPassword: 'testing1234'
            };

            errors = register(user);

            expect(errors).to.deep.equal({
                confirmPassword: 'Incorrect password'
            });
        });

        it('returns multiple errors', () => {
            const user = {
                username: '',
                email: 'dffghdfh',
                password: '',
                confirmPassword: ''
            };

            errors = register(user);

            expect(errors).to.deep.equal({
                username: 'Required',
                email: 'Invalid email address',
                password: 'Required',
                confirmPassword: 'Required'
            })
        });
    });

    describe('address', () => {
        var user;

        beforeEach(() => {
            user = {
                name: 'Jim',
                address1: '8 Lane Road',
                address2: 'Old Town',
                town: 'testing123',
                county: 'testing123',
                country: 'GB',
                postcode: 'CH7 6PN'
            };
        });

        it('returns an empty object if the values are all valid', () => {
            errors = address(user);

            expect(errors).to.deep.equal({});
        });

        checkMissingField('name');
        checkMissingField('address1');
        checkMissingField('town');
        checkMissingField('country');
        checkMissingField('postcode');

        function checkMissingField(field) {
            it('returns ' + field + ' if it is not filled in', () => {
                user[field] = '';
                errors = address(user);

                expect(errors[field]).to.equal('Required');
            });
        }

        it('checks that the country field is a valid country code', () => {
            user.country = 'ZZ';
            errors = address(user);

            expect(errors).to.deep.equal({
                country: 'Invalid value'
            });
        });

        it('returns multiple errors', () => {
            user.address1 = '';
            user.country = '';

            errors = address(user);

            expect(errors).to.deep.equal({
                address1: 'Required',
                country: 'Required'
            })
        });
    });
});