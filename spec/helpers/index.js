export function asyncCheck(callback, done) {
    setTimeout(() => {
        try {
            callback();
            done();
        } catch(e) {
            done(e);
        }
    }, 0);
}