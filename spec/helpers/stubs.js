import sinon from "sinon";

export function getActionStub(method) {
    return sinon.stub().callsFake((...params) => dispatch => {
        dispatch({
            type: method,
            params
        });

        return Promise.resolve();
    });
}

export function stubAction(object, method) {
    sinon.stub(object, method).callsFake((...params) => dispatch => {
        dispatch({
            type: method,
            params
        });

        return Promise.resolve();
    });
}

export function getComposedActionStub(object, method) {
    sinon.stub(object, method).callsFake((...outerParams) => (...innerParams) => dispatch => {
        dispatch({
            type: method,
            outerParams,
            innerParams
        });

        return Promise.resolve();
    });
}