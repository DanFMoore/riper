/**
 * This sets up the Mocha tests to use the JSDOM API as a simulation of the DOM API server-side.
 * Used for the React components tests.
 */

const jsdom = require('jsdom');
const exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom.jsdom('');
global.window = document.defaultView;
jsdom.changeURL(window, "https://example.com/");

Object.keys(document.defaultView).forEach((property) => {
    if (typeof global[property] === 'undefined') {
        exposedProperties.push(property);
        global[property] = document.defaultView[property];
    }
});

global.navigator = {
    userAgent: 'node.js'
};