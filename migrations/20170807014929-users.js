'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('users', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    username: { type: 'string', notNull: true },
    email: { type: 'string', notNull: true },
    joinDate: { type: 'timestamp with time zone', notNull: true },
    password: { type: 'string', notNull: true },
    active: { type: 'boolean', notNull: true, defaultValue: true },
    name: { type: 'string' },
    address1: { type: 'string' },
    address2: { type: 'string' },
    town: { type: 'string' },
    county: { type: 'string' },
    country: { type: 'string' },
    postcode: { type: 'string' }
  });
};

exports.down = function(db) {
  return db.dropTable('users');
};

exports._meta = {
  "version": 1
};
