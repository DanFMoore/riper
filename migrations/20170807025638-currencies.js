'use strict';

var dbm;
var type;
var seed;

var getDone = require('../db').getDone;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, done) {
  done = getDone(done, 3);

  db.createTable('currencies', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    name: { type: 'string', notNull: true },
    code: { type: 'char', notNull: true, length: 3 },
    symbol: { type: 'char', notNull: true, length: 1 }
  }).then(function() {
    db.insert('currencies', ['name', 'code', 'symbol'], ['Pound Sterling', 'GBP', '£'], done);
    db.insert('currencies', ['name', 'code', 'symbol'], ['US Dollar', 'USD', '$'], done);
    db.insert('currencies', ['name', 'code', 'symbol'], ['Euro', 'EUR', '€'], done);
  }).catch(done);
};

exports.down = function(db) {
  return db.dropTable('currencies');
};

exports._meta = {
  "version": 1
};
