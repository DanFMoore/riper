import swig from 'swig';
import bodyParser from 'body-parser';
import { reduxStore } from './redux';
import logger from "morgan";
import path from 'path';
import express from "express";
import session from 'express-session';
import connectPgSimple from 'connect-pg-simple';
const settings = require(process.cwd() + '/resources/settings');

/**
 * Add the common functionality to the main application to facilitate redux
 *
 * @param {Object} app express application
 * @param {Function} reduce
 */
export default function(app, reduce) {
    const db = settings.db;

    app.use(session({
        store: new (connectPgSimple(session))({
            conString: `postgres://${db.user}:${db.password}@${db.host}:${db.port}/${db.database}`
        }),
        secret: settings.cookie.secret,
        resave: false,
        saveUninitialized: true,
        cookie: { secure: settings.cookie.secure }
    }));

    app.engine('html', swig.renderFile);
    app.set('view engine', 'html');
    app.set('views', process.cwd() + '/resources/views');

    app.set('view cache', settings.viewCache);
    app.use(express.static(path.join(process.cwd(), 'public')));

    app.use(logger(settings.logging));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    app.use(reduxStore(reduce));

    app.use(function(err, req, res, next) {
        console.error(err);
        res.status(err.status || 500);

        res.render('error', {
            message: err.message,
            error: settings.env === 'development' ? err : null
        });
    });
}
