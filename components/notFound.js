import React from 'react';
import { Route } from 'react-router-dom';

const Status = ({ code, children }) => (
    <Route render={({ staticContext }) => {
        if (staticContext) {
            staticContext.status = code;
        }

        return children;
    }}/>
);

const NotFound = ({ location }) => (
    <Status code={404}>
        <main className="panel">
            <div className="panel-body">
                <p>The page at <code>{location.pathname}</code> cannot be found.</p>
            </div>
        </main>
    </Status>
);

export default NotFound;