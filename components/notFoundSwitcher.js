import React from 'react';
import { connect } from "react-redux";
import { Route, Switch } from 'react-router-dom';
import NotFound from './notFound';

function notFoundSwitcher({ notFound, children }) {
    return <Switch>
        {notFound ? <Route component={NotFound} /> : [...children, <Route component={NotFound} key="notFound"/>]}
    </Switch>;
}

function mapStateToProps(state) {
    return {
        notFound: state.common.notFound
    };
}

export default connect(mapStateToProps)(notFoundSwitcher);