import React from 'react';
import { connect } from "react-redux";

export class TitleListener extends React.Component {
    componentWillReceiveProps(props) {
        if (this.props.title !== props.title) {
            window.document.title = props.title;
        }
    }

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        title: state.titles.html
    };
}

export default connect(mapStateToProps)(TitleListener);