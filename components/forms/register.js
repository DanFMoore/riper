import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { register as registerValidate } from "../../validators/user";
import { register } from "../../actions/client/users";
import validateOnClient from "../../validators/client";
import renderField from "../renderField";

export function Register(props) {
    const { handleSubmit, pristine, submitting } = props;

    return <form
        onSubmit={handleSubmit(props.register)}
        className="register-form"
    >
        <Field name="username" type="text" component={renderField} label="Username" />
        <Field name="email" type="email" component={renderField} label="Email Address" />
        <Field name="password" type="password" component={renderField} label="Password" />
        <Field name="confirmPassword" type="password" component={renderField} label="Confirm Password" />

        <div className="form-group">
            <button type="submit" disabled={pristine || submitting} className="btn btn-primary">
                Register
            </button>
        </div>
    </form>;
}

const form = reduxForm({
    form: 'register',
    validate: registerValidate,
    asyncValidate: validateOnClient('/users'),
    asyncBlurFields: ['username', 'email']
})(Register);

export default connect(null, { register })(form);