import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { login as loginValidate } from "../../validators/user";
import { login } from "../../actions/client/users";
import renderField from "../renderField";

export function Login(props) {
    const errorMessage = props.loginFailureMessage ? <div className="alert alert-danger">
        { props.loginFailureMessage }
    </div> : null;

    return <form
        onSubmit={props.handleSubmit(props.overrideLogin || props.login)}
        className="login-form"
    >
        { errorMessage }

        <Field name="username" type="text" component={renderField} label="Username" />
        <Field name="password" type="password" component={renderField} label="Password" />

        <div className="form-group">
            <button type="submit" disabled={props.submitting} className="btn btn-primary">Login</button>
        </div>
    </form>;
}

const form = reduxForm({
    form: 'login',
    validate: loginValidate
})(Login);

function mapStateToProps(state) {
    return state.users;
}

export default connect(mapStateToProps, { login })(form);