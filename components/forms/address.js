import React from "react";
import { Field } from "redux-form";
import renderField from "../renderField";
import countries from "../../resources/countries.json";

/**
 * This returns just the fields not the form itself as the handler needs to accept additional things
 */
export default function Address({ inactive, values }) {
    return inactive ?
        <div>
            <p>{values.address1}</p>
            {values.address2 ? <p>{values.address2}</p> : null}
            <p>{values.town}</p>
            {values.county ? <p>{values.county}</p> : null}
            <p>{values.postcode}</p>
            <p>{countries[values.country]}</p>
        </div>
        : <div>
        <Field name="address1" type="text" component={renderField} label="Address Line 1"
               required={true} />
        <Field name="address2" type="text" component={renderField} label="Address Line 2"  />
        <Field name="town" type="text" component={renderField} label="Town"  required={true} />
        <Field name="county" type="text" component={renderField} label="County" />
        <Field name="postcode" type="text" component={renderField} label="Postcode"
               required={true} />
        <Field name="country" component={renderField} type="select" label="Country"
               required={true} options={countries} />
    </div>;
}