import React from "react";
import { Field } from "redux-form";
import renderField from "../renderField";

/**
 * This returns just the fields not the form itself as the handler needs to accept additional things
 */
export default function CardDetails() {
    return <div>
        <Field name="card-number" type="text" component={renderField} label="Card Number"
               required={true} />
        <Field name="expiry" type="text" component={renderField} label="Expiry Date" required={true} />
        <Field name="code" type="number" component={renderField} label="Security Code" required={true} />
    </div>;
}