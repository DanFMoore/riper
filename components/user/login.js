import React from "react";
import { connect } from "react-redux";
import { Redirect, Link } from 'react-router-dom';
import { logout } from "../../actions/client/users";
import * as types from "../../actions/types";
import Form from "../forms/login";

export class Login extends React.Component {
    componentWillMount() {
        this.props.logout();
    }

    render() {
        if (this.props.actionType === types.USER_AUTH && this.props.loggedInUser) {
            return <Redirect to="/user/profile" />;
        }

        return <main className="panel">
            <div className="panel-body">
                <Form />

                <p>Don't have an account? <Link to="/user/register">Register here</Link></p>
            </div>
        </main>;
    }
}

function mapStateToProps(state) {
    return state.users;
}

export default connect(mapStateToProps, { logout })(Login);