import React from "react";
import { Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import { logout } from "../../actions/client/users";

class Logout extends React.Component {
    componentWillMount() {
        this.props.logout();
    }

    render() {
        return <Redirect to="/user/login" />;
    }
}

export default connect(null, { logout })(Logout);