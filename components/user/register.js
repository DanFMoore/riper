import React from "react";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { register, logout } from "../../actions/client/users";
import * as types from "../../actions/types";
import Form from "../forms/register";

export class Register extends React.Component {
    componentWillMount() {
        this.props.logout();
    }

    render() {
        if (this.props.actionType === types.USER_REGISTER) {
            return <main className="panel">
                <div className="registered panel-body">
                    <p>You have successfully registered.</p>
                    <p><Link to="/user/profile">Click here</Link> to view your profile.</p>
                </div>
            </main>;
        }

        return <main className="panel">
            <div className="panel-body">
                <Form />

                <p>Already have an account? <Link to="/user/login">Log in here</Link></p>
            </div>
        </main>;
    }
}

function mapStateToProps(state) {
    return state.users;
}

export default connect(mapStateToProps, { logout, register })(Register);