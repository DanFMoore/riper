import countries from "../resources/countries.json";
import moment from "moment";

/**
 * @todo handle other currencies
 */
export function price(value) {
    if (value) {
        return '£' + Number(value).toFixed(2);
    }
}

export function date(value) {
    if (value) {
        return moment(value).format('ll');
    }
}

export function dateTime(value) {
    if (value) {
        return moment(value).format('lll');
    }
}

export function explodeAddress(address) {
    const result = [
        address.address1,
        address.address2,
        address.town,
        address.county,
        address.postcode,
        address.country ? countries[address.country] : null
    ];

    return result.filter(Boolean);
}