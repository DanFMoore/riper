import React from 'react';

export default ({input, label, type, options = {}, defaultValue = undefined, noLabel = false, required = false, meta: {
    touched, error, warning
}}) => {
    function renderElement() {
        if (type === 'select') {
            return renderSelect();
        } else {
            return <input {...input} type={type} className="form-control"/>;
        }
    }

    function renderSelect() {
        return <select {...input} className="form-control" defaultValue={defaultValue} >
            {required ? null : <option />}
            {Object.keys(options).map(key => {
                return <option key={key} value={key}>{options[key]}</option>;
            })}
        </select>;
    }

    const element = noLabel ? renderElement() : <label>
        <span>{label} {required ? <span className="required">*</span> : ''}</span>
        {renderElement()}
    </label>;

    return <div className={(touched && (error || warning) ? 'has-error' : '') + ' form-group'}>
        {element}
        {touched && (error || warning) ? <ul className="errors">
            {touched &&
            ((error && <li>{error}</li>) ||
            (warning && <li>{warning}</li>))}
        </ul> : null}
    </div>
};

