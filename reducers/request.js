import * as types from "../actions/types";

export default function reduceRequest(state = {
    errors: {},
    invalid: false
}, action) {
    switch (action.type) {
        case types.REQUEST_STORE_BODY:
            return {
                ...state,
                body: action.body
            };

        case types.REQUEST_VALIDATE:
            return {
                ...state,
                errors: {
                    ...action.errors,
                    ...state.errors
                },
                invalid: Boolean(state.invalid || Object.keys(action.errors || {}).length)
            };
    }

    return state;
}