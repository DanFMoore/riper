/**
 * Add the related records from source to state
 *
 * @param {Array|Object} state
 * @param {Array} source
 * @param {String} property
 * @param {String} foreignKey
 * @param {Boolean} inverse
 * @param {Boolean} array
 * @returns {Array}
 */
export function addRelatedRecords(state, source, property, foreignKey, inverse = false, array = false) {
    function addToSingleStateRecord(stateRecord) {
        stateRecord = { ...stateRecord };

        const filteredRecords = source.filter(
            sourceRecord => inverse ?
            stateRecord.id == sourceRecord[foreignKey] :
            sourceRecord.id == stateRecord[foreignKey]
        );

        if (array) {
            stateRecord[property] = filteredRecords;
        } else {
            stateRecord[property] = filteredRecords[0] || stateRecord[property];
        }

        return stateRecord;
    }

    if (state.constructor === Array) {
        return state.map(addToSingleStateRecord);
    } else {
        return addToSingleStateRecord(state);
    }
}

/**
 * Tested indirectly in reducers/productsSpec.js
 *
 * @param {Array} records
 * @param {String} foreignKey
 * @returns {Array}
 */
export function getForeignIds(records, foreignKey) {
    const ids = records.map(record => record[foreignKey]).filter(Boolean);

    return [ ...new Set(ids) ];
}