import * as types from "../actions/types";

export default function reduceCommon(state = {}, action) {
    switch (action.type) {
        case types.ERROR:
            return {
                ...state,
                error: action.error
            };

        case types.NOT_FOUND:
            return {
                ...state,
                notFound: true
            };

        case types.CLEAR_ERROR:
            return {
                ...state,
                error: null,
                notFound: false
            };
    }

    return state;
}