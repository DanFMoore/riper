import * as types from "../actions/types";

function updateInRecords(records, id, actionRecord) {
    if (records) {
        return records.map(record => {
            if (record.id === id) {
                return {
                    ...record,
                    ...actionRecord
                };
            }

            return record;
        });
    }
}

function updateRecord(record, id, actionRecord) {
    if (record && record.id === id) {
        return {
            ...record,
            ...actionRecord
        }
    }

    return record;
}

function deleteInRecords(records, id) {
    if (records) {
        return records.filter(record => id !== record.id);
    }
}

function deleteRecord(record, id) {
    if (record && record.id === id) {
        return undefined;
    }

    return record;
}

export default function getRepositoryReducer(repository) {
    return function reduceRepositories(state = {
        records: [],
        orderColumns: {},
        pagination: {}
    }, action) {
        if (!action.repository || action.repository !== repository) {
            return state;
        }

        switch(action.type) {
            case types.REPOSITORY_GET_RECORDS:
                return {
                    ...state,
                    records: action.records,
                    orderColumns: action.orderColumns || {},
                    pagination: action.pagination ? {
                        ...action.pagination,
                        pageNo: Number(action.pagination.pageNo)
                    } : {}
                };

            case types.REPOSITORY_GET_RECORD:
                return {
                    ...state,
                    record: action.record
                };

            case types.REPOSITORY_CREATE_RECORD:
                return {
                    ...state,
                    createdRecord: {
                        ...action.record,
                        id: action.id
                    }
                };

            case types.REPOSITORY_UPDATE_RECORD:
                return {
                    ...state,
                    updatedRecord: {
                        ...action.record,
                        id: action.id
                    },
                    records: updateInRecords(state.records, action.id, action.record),
                    record: updateRecord(state.record, action.id, action.record)
                };

            case types.REPOSITORY_DELETE_RECORD:
                return {
                    ...state,
                    deletedRecord: {
                        ...action.record,
                        id: action.id
                    },
                    records: deleteInRecords(state.records, action.id),
                    record: deleteRecord(state.record, action.id)
                };
        }

        return state;
    };
};