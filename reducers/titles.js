import * as types from "../actions/types";

export default function getReducer(siteName) {
    return function reduceTitles(state = {
        siteName,
        title: '',
        html: siteName
    }, action) {
        if (action.type === types.TITLE_SET) {
            return {
                ...state,
                title: action.title,
                html: action.html || action.title + ' - ' + state.siteName
            };
        }

        return state;
    }
};
