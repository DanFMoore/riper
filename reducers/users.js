import * as types from "../actions/types";
import getReducer from "./repositories";

export default function reduceUsers(state = {
    records: [],
    save: true
}, action) {
    switch(action.type) {
        case types.USERNAME_NOT_FOUND:
            return {
                ...state,
                loginFailureMessage: 'Username not found',
                loggedInUser: null,
                token: null,
                actionType: action.type
            };

        case types.USER_TOKEN_EXPIRED:
            return {
                ...state,
                loginFailureMessage: 'User session expired',
                loggedInUser: null,
                token: null,
                actionType: action.type
            };

        case types.USER_REGISTER:
            return {
                ...state,
                actionType: action.type
            };

        case types.USER_AUTH:
            if (action.loggedInUser) {
                return {
                    ...state,
                    loginFailureMessage: null,
                    loggedInUser: action.loggedInUser,
                    token: action.token,
                    actionType: action.type
                };
            } else {
                return {
                    ...state,
                    loginFailureMessage: 'Incorrect password',
                    loggedInUser: null,
                    token: null,
                    actionType: action.type
                };
            }

        case types.USER_LOGOUT:
            return {
                ...state,
                loginFailureMessage: null,
                loggedInUser: null,
                token: null,
                actionType: action.type
            };

        case types.REPOSITORY_UPDATE_RECORD:
            if (action.repository === 'users' && state.loggedInUser && state.loggedInUser.id === action.id) {
                return {
                    ...state,
                    loggedInUser: {
                        ...state.loggedInUser,
                        ...action.record,
                        id: action.id
                    }
                };
            }
    }

    return getReducer('users')(state, action);
}