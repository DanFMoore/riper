import * as types from "../actions/types";

export default function reduceModals(state = {}, action) {
    switch (action.type) {
        case types.MODAL_SHOW: {
            const newState = {...state};

            Object.keys(newState).forEach(i => {
                newState[i] = false;
            });

            newState[action.modal] = true;

            return newState;
        }

        case types.MODAL_HIDE: {
            const newState = {...state};
            newState[action.modal] = false;

            return newState;
        }

        case types.MODAL_HIDE_ALL:
            return {};
    }

    return state;
}