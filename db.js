"use strict";

/**
 * This file has been converted from ES2015 into basic JS. This is so that it can be used in migrations.
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.connect = connect;
exports.initDb = initDb;
exports.getDone = getDone;
exports.seed = seed;

var pgp = require("pg-promise");
var types = require("pg").types;
var dbMigrate = require("db-migrate");
var exec = require('child_process').execSync;
var settings = require(process.cwd() + '/resources/settings');

function connect() {
    // Convert "numeric" pg type to numbers
    types.setTypeParser(1700, Number);

    // Convert the date fields back to iso strings. Makes it more consistent when JSON'd
    function convertDate(date) {
        if (date && date.constructor === Date) {
            return date.toISOString();
        }

        return date;
    }

    types.setTypeParser(1082, convertDate);
    types.setTypeParser(1114, convertDate);
    types.setTypeParser(1184, convertDate);

    return pgp()(settings.db);
}

/**
 * Import the base table structure and an additional fixture file if specified
 *
 * @param {String} [fixture]
 */
function initDb() {
    var fixture = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var dbName = settings.db.database;

    try {
        exec('createdb ' + dbName);
    } catch (e) {
        console.error(e.message);
    }

    try {
        exec("psql -c 'DROP SCHEMA public CASCADE;' -d " + dbName);
        exec("psql -c 'CREATE SCHEMA public;' -d " + dbName);
        exec("psql -c 'GRANT ALL ON SCHEMA public TO postgres;' -d " + dbName);
        exec("psql -c 'GRANT ALL ON SCHEMA public TO public;' -d " + dbName);
    } catch (e) {
        console.error(e.message);
    }

    var dbmigrate = dbMigrate.getInstance(true);

    return dbmigrate.up().then(function (result) {
        if (fixture) {
            exec("psql " + dbName + " < " + fixture);
        }

        return result;
    });
}

/**
 * In a migration, get a wrapper for a done function for calling it multiple times.
 *
 * @param {Function} done callback passed into exports.up()
 * @param {Number} count amount of times to run
 * @returns {Function}
 */
function getDone(done, count) {
    var doneCount = 0;

    // Bit of a hack since db-migrate only supports promises on some methods. But not insert.
    // Also insert is deprecated, but fuck setting up seeds, when this is "system" data and so
    // more like structure than customer data.
    return function (err) {
        if (err) {
            return done(err);
        }

        doneCount++;

        if (doneCount === count) {
            return done();
        }
    };
}


/**
 * Run the passed in seeds in order after truncating the database
 *
 * @param {Function} getSeeds
 * @return {Promise}
 */
function seed(getSeeds) {
    return initDb().then(function () {
        var seeds = getSeeds();

        var seedKeys = Object.keys(seeds);
        var promise = seeds[seedKeys[0]]();

        for (var i = 0; i < seedKeys.length; i++) {
            (function (i) {
                promise = promise.then(function () {
                    console.log('Imported ' + seedKeys[i]);

                    if (seedKeys[i + 1]) {
                        return seeds[seedKeys[i + 1]]();
                    }
                });
            })(i);
        }

        return promise;
    }).then(function () {
        console.log('Imported all seeds');
        process.exit();
    }).catch(console.error);
}